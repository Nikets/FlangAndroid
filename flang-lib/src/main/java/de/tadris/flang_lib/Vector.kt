package de.tadris.flang_lib

import de.tadris.flang_lib.Board

open class Vector(val x: Int, val y: Int){

    fun reverseY(): Vector{
        return Vector(x, -y)
    }

    open fun add(other: Vector): Vector {
        return Vector(x+other.x, y+other.y)
    }

    fun getNotation(): String{
        return ('A'.toInt() + x).toChar() + (y+1).toString()
    }

    override fun toString(): String {
        return getNotation()
    }

    open fun toLocation(board: Board): Location {
        return Location(board, x, y)
    }

    fun isValid(): Boolean {
        return x >= 0 && y >= 0 && x < Board.BOARD_SIZE && y < Board.BOARD_SIZE
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vector) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }


    companion object {

        fun parse(str: String): Vector { // String like 'B4'
            assert(str.length == 2)
            val x = str[0].toInt() - 'A'.toInt()
            val y = str[1].toString().toInt() - 1
            return Vector(x, y)
        }

    }

}