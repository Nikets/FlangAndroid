package de.tadris.flang_lib

import de.tadris.flang_lib.bot.FastMoveGenerator
import java.io.File
import java.lang.StringBuilder

fun main() {
    val file = File("move_map_eval.txt")
    val useEvalMode = true


    file.writeText("")

    Type.values().forEach { type ->
        println("$type")
        for(x in 0 until 8){
            for(y in 0 until 8){
                val board = emptyBoard()
                val baseLocation = Location(board, x, y)
                val piece = Piece(baseLocation, type, Color.BLACK, PieceState.NORMAL)

                val directions = piece.getPossibleDirections().toMutableList()

                if(piece.type == Type.KING && useEvalMode){
                    directions.clear()
                    for(x in -2..2){
                        for(y in -2..2){
                            directions.add(listOf(Vector(x, y)))
                        }
                    }
                }
                directions.forEach { ray ->
                    board.setOnBoard(baseLocation, piece, Board.FreezeMethod.UNFREEZE)
                    val sb = StringBuilder()
                    sb.append("${type.getChar(Color.BLACK)}" + baseLocation.getNotation() + ":")
                    var empty = true
                    var rayEnded = false
                    ray.forEach { targetVector ->
                        if(!rayEnded){
                            val target = baseLocation.add(targetVector)
                            if(target.isValid()){
                                if(empty){
                                    empty = false
                                }else{
                                    sb.append(",")
                                }
                                sb.append(target.getNotation())
                            }else{
                                rayEnded = true
                            }
                        }
                    }
                    sb.append("\n")
                    if(!empty){
                        file.appendText(sb.toString())
                    }
                }
            }
        }
    }

}


fun emptyBoard(): Board {
    return Board(" ".repeat(8*8))
}