package de.tadris.flang_lib

import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import java.lang.IllegalArgumentException
import java.lang.StringBuilder

class Board(val pieces: CharArray, var atMove: Color = Color.WHITE, var moveNumber: Int = 0, actionList: ActionList? = null) {

    val moveList = actionList?: ActionList(this)
    var resigned: Color? = null
    var isInfiniteGame = false

    companion object {
        const val BOARD_SIZE = 8
        const val EMPTY = ' '
        const val ARRAY_SIZE = BOARD_SIZE * BOARD_SIZE
        const val FULL_ARRAY_SIZE = ARRAY_SIZE * 2

        const val DEFAULT_BOARD = "  PRHFUK" +
                "  PPPPPP" +
                "        " +
                "        " +
                "        " +
                "        " +
                "pppppp  " +
                "kufhrp  "

        private fun parse(str: String): CharArray{
            return when (str.length) {
                FULL_ARRAY_SIZE -> {
                    str.toCharArray()
                }
                ARRAY_SIZE -> {
                    val sb = StringBuilder()
                    str.forEach {
                        sb.append(it)
                        sb.append(PieceState.NORMAL.char)
                    }
                    sb.toString().toCharArray()
                }
                else -> {
                    throw IllegalArgumentException("Size of a board must be $FULL_ARRAY_SIZE (size was ${str.length})")
                }
            }
        }

    }

    constructor(str: String, atMove: Color = Color.WHITE) : this(parse(str), atMove)

    fun getAt(index: Int): Piece? {
        if(index < 0){ return null }
        val char = pieces[index]
        return if(char == EMPTY){
            null
        }else{
            Piece(getLocationForIndex(index), char, pieces[index + 1])
        }
    }

    fun getAt(loc: Vector): Piece? {
        return getAt(getIndexOfLocation(loc))
    }

    fun getIndexOfLocation(loc: Vector): Int{
        return (loc.y * BOARD_SIZE + loc.x) * 2
    }

    fun getLocationForIndex(index: Int): Location {
        val halfIndex = index / 2
        return Location(this, halfIndex % BOARD_SIZE, halfIndex / BOARD_SIZE)
    }

    fun gameIsComplete(): Boolean{
        return !isInfiniteGame && (hasWon(Color.WHITE) || hasWon(Color.BLACK) || resigned != null)
    }

    fun hasWon(color: Color): Boolean{
        if(color.getOpponent() == resigned) return true // If opponent has resigned, it's a win
        val king = findKing(color) ?: return false // has no king, so hasn't won
        return if(findKing(color.getOpponent()) == null){
            true // opponent has no king, so won
        }else king.location.y == color.winningY
    }

    fun findKing(color: Color): Piece? {
        return findFigure(Type.KING, color)
    }

    fun findFigure(type: Type, color: Color): Piece? {
        return getAt(pieces.indexOf(type.getChar(color)))
    }

    fun eachLocation(action: (Location) -> Unit){
        for(y in 0 until BOARD_SIZE){
            for(x in 0 until BOARD_SIZE){
                action(Location(this, x, y))
            }
        }
    }

    fun eachPiece(color: Color?, action: (Piece) -> Unit){
        eachLocation {
            val piece = it.piece.value
            if(piece != null){
                if(color == null || piece.color == color){
                    action(piece)
                }
            }
        }
    }

    fun executeOnBoard(move: Move): Board {
        val piece = move.piece
        unfreezeAllOnBoard(piece.color)
        clearOnBoard(piece.location)
        setOnBoard(move.target, piece)
        atMove = atMove.getOpponent()
        moveNumber++
        moveList.actionList.add(move)
        return this
    }

    fun unfreezeAllOnBoard(color: Color){
        val upperCase = color == Color.WHITE
        for(index in 0 until FULL_ARRAY_SIZE step 2){
            val state = pieces[index+1]
            if(state == PieceState.FROZEN.char){
                val piece = pieces[index]
                if(piece.isUpperCase() == upperCase){
                    pieces[index+1] = PieceState.NORMAL.char
                }
            }
        }
    }

    fun executeOnNewBoard(move: Move): Board {
        return clone(false).executeOnBoard(move)
    }

    fun clone(slowButComplete: Boolean): Board {
        return if(slowButComplete){
            val board = Board(pieces.clone(), atMove, moveNumber, moveList.clone())
            board.isInfiniteGame = isInfiniteGame
            board
        }else{
            Board(pieces.clone(), atMove, moveNumber)
        }
    }

    fun clearOnBoard(location: Location){
        setOnBoard(location, null)
    }

    fun setOnBoard(location: Location, piece: Piece?, freezeMethod: FreezeMethod = FreezeMethod.AUTO){
        val index = getIndexOfLocation(location)
        if(piece != null){
            pieces[index] = piece.getChar()
            pieces[index+1] = (
                    if(freezeMethod != FreezeMethod.UNFREEZE){
                        if(freezeMethod == FreezeMethod.FORCE || piece.type.hasFreeze){
                            PieceState.FROZEN
                        }else{
                            PieceState.NORMAL
                        }
                    }else {
                        PieceState.NORMAL
                    }).char
            if(piece.type == Type.PAWN &&
                    (if(piece.color == Color.WHITE) BOARD_SIZE-1-location.y else location.y) == 0){
                setOnBoard(location, Piece(location, Type.UNI, piece.color, piece.state), FreezeMethod.AUTO)
            }
        }else{
            pieces[index] = EMPTY
            pieces[index+1] = PieceState.NORMAL.char
        }
    }

    fun findAllMoves(color: Color): List<Move>{
        if(gameIsComplete()){
            return emptyList()
        }
        val list = mutableListOf<Move>()

        eachPiece(color) {
            list.addAll(it.getPossibleMoves())
        }

        return list
    }

    fun getFBN(): String { // Flang board notation - contains current state
        return pieces.concatToString()
    }

    fun getFMN(): String { // Flang move notation - contains all data
        return moveList.toString()
    }

    override fun toString(): String {
        return getFBN()
    }

    fun printBoard(){
        println("+-----------------+")
        for(y in BOARD_SIZE -1 downTo 0){
            print("| ")
            for(x in 0 until BOARD_SIZE){
                print(pieces[(y*8 + x)*2] + " ")
            }
            println("|")
        }
        println("+-----------------+")
    }

    enum class FreezeMethod {
        AUTO, // Depending on piece type
        FORCE, // Force freeze piece
        UNFREEZE // Dont freeze
    }
}