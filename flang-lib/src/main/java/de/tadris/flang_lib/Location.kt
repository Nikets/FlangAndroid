package de.tadris.flang_lib


class Location(val board: Board, x: Int, y: Int) : Vector(x, y) {

    val piece = lazy { board.getAt(this) }

    override fun add(other: Vector): Location {
        return Location(board, x+other.x, y+other.y)
    }

    override fun toLocation(board: Board): Location {
        return this
    }

    fun onDifferentBoard(board: Board): Location {
        return Location(board, x, y)
    }
}