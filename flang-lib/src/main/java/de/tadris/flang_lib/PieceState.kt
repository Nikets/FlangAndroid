package de.tadris.flang_lib

enum class PieceState(val char: Char){

    NORMAL('+'),
    FROZEN('-');


    companion object {

        fun getState(char: Char): PieceState {
            return values().find { it.char == char } ?: NORMAL
        }

    }

}