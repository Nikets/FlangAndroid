package de.tadris.flang_lib
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.BoardEvaluation
import de.tadris.flang_lib.bot.FlangBot

fun main() {

    //val board = Board(Board.DEFAULT_BOARD)

    //val board = Board(" + +P+ +R+ +U+ + + +P+P+ +P+P+ + + + +H+P+ + + + + + +K+ + + + + +p+ + + + +u+ +k+ + + + + +p+ + +p+p+ +p+h+p+ + + + + + + + +r+")

    val board = Board(" + +P+R+ +F+ + + + +P+ +P+ +P+ + + + + + +H+ + + + + + + + + +K+ + +p+ + + +P- + + + +p-h+ +p+ +p+ +p+ +p+p+ + +k+ +f+ + + + + +")

    //val board = Board(" + + + + + + + + +R+ + +U+ +P+ +k+ + +H+ + +P+K+ + + +P+ + + + + + + + + + +p- + + + + + + + + +p+ +p+p+ +p+ + + + + +h+ +p+ + +", Color.BLACK)

    println(board.findAllMoves(Color.WHITE).joinToString(separator = "\n"))
    println(board.findAllMoves(Color.WHITE).size)


        /*de.tadris.flang_lib.Board("K       " +
            "PPPPPPPP" +
            "        " +
            "        " +
            "        " +
            "      p " +
            "pppppp p" +
            "       k")*/

    // Bot 1 is white, bot2 is black

    //val bot1 = FlangBot(4, 4)
    val bot1 = FlangBot(1, 4)

    board.printBoard()

    BoardEvaluation(board).printMatrix()

    while (true){
        board.printBoard()
        println(board.getFBN())
        BoardEvaluation(board).printMatrix()

        println("BOT 1")
        val result = bot1.findBestMove(board)
        println(result.evaluations)
        val move1 = result.bestMove.move
        println("Evaluated moves: ${bot1.evaluations}")
        println("Move: $move1")
        board.executeOnBoard(move1)

        if(board.hasWon(Color.WHITE)){
            println("White has won")
        }
        if(board.hasWon(Color.BLACK)){
            println("Black has won")
        }

        board.printBoard()
        println(board.getFBN())
        BoardEvaluation(board).printMatrix()

        var done = false
        while (!done){
            try {
                Thread.sleep(500)
                print("> ")
                val nextMove = readLine()!!
                val userMove = Move.parse(board, " $nextMove")
                board.executeOnBoard(userMove)
                done = true
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

        board.printBoard()
        println(board.getFBN())
        BoardEvaluation(board).printMatrix()
    }

}