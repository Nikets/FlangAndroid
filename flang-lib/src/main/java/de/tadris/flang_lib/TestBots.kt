package de.tadris.flang_lib
import de.tadris.flang_lib.bot.BoardEvaluation
import de.tadris.flang_lib.bot.FlangBot
import de.tadris.flang_lib.bot.FlangBot2
import kotlin.system.exitProcess

fun main() {

    val board = Board(Board.DEFAULT_BOARD)

    //val board = de.tadris.flang_lib.Board(" + +P+ +R+ +U+ + + +P+P+ +P+P+ + + + +H+P+ + + + + + +K+ + + + + +p+ + + + +u+ +k+ + + + + +p+ + +p+p+ +p+h+p+ + + + + + + + +r+")

    //val board = Board(" + + + + + + + + + + + + +P+P+ + + + +P+ + + + + + + +P+F+ + +K+ +p+ + + + +u- + +p+ +p+ + +p+ + +k+ + +p+ +p+ + + + + + + + + +")

        /*de.tadris.flang_lib.Board("K       " +
            "PPPPPPPP" +
            "        " +
            "        " +
            "        " +
            "      p " +
            "pppppp p" +
            "       k")*/

    // Bot 1 is white, bot2 is black

    val bot1 = FlangBot(4, 4)
    val bot2 = FlangBot2(4, 4)

    var sum1 = 0
    var sum2 = 0

    board.printBoard()

    BoardEvaluation(board).printMatrix()

    while (true){
        board.printBoard()
        println(board.getFBN())
        BoardEvaluation(board).printMatrix()

        println("BOT 1")
        val result = bot1.findBestMove(board)
        println(result.evaluations)
        sum1+= bot1.evaluations
        val move1 = result.bestMove.move
        println("Evaluated moves: ${bot1.evaluations}")
        println("Move: $move1")
        board.executeOnBoard(move1)

        if(board.hasWon(Color.WHITE)){
            println("White has won")
            println("Eval 1: $sum1 / Eval 2: $sum2")
            exitProcess(0)
        }
        if(board.hasWon(Color.BLACK)){
            println("Black has won")
            println("Eval 1: $sum1 / Eval 2: $sum2")
            exitProcess(0)
        }

        board.printBoard()
        println(board.getFBN())
        BoardEvaluation(board).printMatrix()

        println("BOT 2")
        val result2 = bot2.findBestMove(board)
        println(result.evaluations)
        sum2+= bot2.evaluations
        val move2 = result2.bestMode.move
        println("Evaluated moves: ${bot2.evaluations}")
        println("Move: $move2")
        board.executeOnBoard(move2)

        if(board.hasWon(Color.WHITE)){
            println("White has won")
            println("Eval 1: $sum1 / Eval 2: $sum2")
            exitProcess(0)
        }
        if(board.hasWon(Color.BLACK)){
            println("Black has won")
            println("Eval 1: $sum1 / Eval 2: $sum2")
            exitProcess(0)
        }

        board.printBoard()
        println(board.getFBN())
        println(board.getFMN())
        BoardEvaluation(board).printMatrix()
    }

}