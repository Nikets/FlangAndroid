package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Type
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class FlangBot(val minDepth: Int, val maxDepth: Int) {

    var evaluations = 0
    var minMoveNumber = 0
    var bestMap: MutableMap<Int, MoveEvaluation> = mutableMapOf()

    fun findBestMove(board: Board) : BotResult {
        val start = System.currentTimeMillis()
        evaluations = 0
        minMoveNumber = 0
        bestMap.clear()
        val eval = findBestMove(board, maxDepth)
        val end = System.currentTimeMillis()
        println((end-start).toString() + "ms")
        return eval!!
    }

    private fun findBestMove(board: Board, depth: Int) : BotResult? {
        val moveEvaluations = mutableListOf<MoveEvaluation>()
        val allMoves = board.findAllMoves(board.atMove).toMutableList()
        allMoves.sortBy { BoardEvaluation(board.executeOnNewBoard(it)).evaluate()*board.atMove.evaluationNumber }
        allMoves.forEach {
            moveEvaluations+= MoveEvaluation(it, evaluateMove(board.executeOnNewBoard(it), depth-1, -100000.0, 100000.0), depth)
        }

        moveEvaluations.shuffle()
        moveEvaluations.sortBy { -((it.evaluation*100).roundToInt() / 100.0)*board.atMove.evaluationNumber }
        if(moveEvaluations.size == 0){
            return null
        }
        val bestMove = moveEvaluations[0]

        return BotResult(MoveEvaluation(bestMove.move, bestMove.evaluation, depth), moveEvaluations, evaluations)
    }

    private fun evaluateMove(board: Board, depth: Int, alpha: Double, beta: Double) : Double {
        var alpha = alpha
        var beta = beta
        if(depth <= 0){
            evaluations++
            return BoardEvaluation(board).evaluate()
        }

        var bestEvaluation = -1000000.0*board.atMove.evaluationNumber

        val allMoves = board.findAllMoves(board.atMove).toMutableList()
        if(depth >= 3){
            allMoves.sortBy {
                -evaluateMove(board.executeOnNewBoard(it), 0, 0.0, 0.0)*board.atMove.evaluationNumber
            }
        }else if(depth >= 2){
            allMoves.sortBy {
                return@sortBy when(it.piece.type){
                    Type.KING -> -1
                    else -> 0
                }
            }
        }

        allMoves.forEach { move ->
            val hypotheticalBoard = board.executeOnNewBoard(move)
            val moveEvaluation = evaluateMove(hypotheticalBoard, depth - 1, alpha, beta)*0.99

            if(board.atMove == Color.WHITE){
                bestEvaluation = max(bestEvaluation, moveEvaluation)
                alpha = max(alpha, bestEvaluation)
                if(alpha >= beta){
                    return bestEvaluation
                }
            }else{
                bestEvaluation = min(bestEvaluation, moveEvaluation)
                beta = min(beta, bestEvaluation)
                if(beta <= alpha){
                    return bestEvaluation
                }
            }
        }

        return bestEvaluation
    }

    class BotResult(val bestMove: MoveEvaluation, val evaluations: List<MoveEvaluation>, val count: Int)

}