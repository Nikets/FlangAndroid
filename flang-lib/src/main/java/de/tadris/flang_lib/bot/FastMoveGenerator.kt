package de.tadris.flang_lib.bot

import de.tadris.flang_lib.*

class FastMoveGenerator(private val board: Board) {

    // For REAL target locations use piece.getTargets()
    // This returns a greater influence for the king
    fun getTargetLocations(piece: Piece): List<Location> {
        if(piece.state == PieceState.FROZEN){
            return emptyList()
        }
        return when(piece.type){
            Type.PAWN -> getMovesForPawn(piece)
            Type.KING -> getMovesForKing(piece)
            else -> getPossibleTargetLocations(piece)
        }
    }

    private fun getMovesForPawn(piece: Piece): List<Location>{
        // Optimized method for pawns
        val list = mutableListOf<Location>()

        val yDirection = piece.color.evaluationNumber

        if(checkTarget(piece.location.x, piece.location.y+yDirection, piece.color)){
            list.add(Location(board, piece.location.x, piece.location.y+yDirection))
        }
        if(checkTarget(piece.location.x+1, piece.location.y+yDirection, piece.color)){
            list.add(Location(board, piece.location.x+1, piece.location.y+yDirection))
        }
        if(checkTarget(piece.location.x-1, piece.location.y+yDirection, piece.color)){
            list.add(Location(board, piece.location.x-1, piece.location.y+yDirection))
        }

        return list
    }

    private fun getMovesForKing(piece: Piece): List<Location>{
        // Optimized method for kings, DOUBLE RANGE
        val list = mutableListOf<Location>()

        for(x in -2..2){
            for(y in -2..2){
                if(checkLocation(piece.location.x+x, piece.location.y+y)){
                    list.add(Location(board, piece.location.x+x, piece.location.y+y))
                }
            }
        }

        return list
    }

    private fun getPossibleTargetLocations(piece: Piece): List<Location> {
        val targets = mutableListOf<Location>()

        piece.type.moves.forEach { batch ->
            getPossibleTargetLocationForBatch(piece, batch).forEach {
                if(!piece.type.hasDoubleMoves || !targets.contains(it)){
                    targets.add(it)
                }
            }
        }
        return targets
    }

    private fun getPossibleTargetLocationForBatch(piece: Piece, batch: List<Vector>): List<Location> {
        val targets = mutableListOf<Location>()

        batch.forEach {
            val target = piece.location.add(it)
            if(target.isValid()){
                targets.add(target)
                if(!isEmpty(target.x, target.y)){
                    return targets
                }
            }else{
                return targets
            }
        }
        return targets
    }

    fun checkTarget(x: Int, y: Int, color: Color): Boolean {
        return checkLocation(x, y) && (isEmpty(x, y) || (isWhite(x, y) != (color == Color.WHITE)))
    }

    fun checkLocation(x: Int, y: Int): Boolean {
        return x >= 0 && y >= 0 && x < Board.BOARD_SIZE && y < Board.BOARD_SIZE
    }

    fun isEmpty(x: Int, y: Int): Boolean {
        return getChar(x, y) == Board.EMPTY
    }

    fun isWhite(x: Int, y: Int): Boolean{
        return getChar(x, y).isUpperCase()
    }

    fun getChar(x: Int, y: Int): Char {
        return board.pieces[(y * Board.BOARD_SIZE + x) * 2]
    }

}