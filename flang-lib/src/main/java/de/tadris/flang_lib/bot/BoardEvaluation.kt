package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Vector
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.pow

/**
 * For all evaluations: positive stands for white and negative for black
 *
 * This method is called "Neo method" (Matrix evaluation)
 */
class BoardEvaluation(private val board: Board) {

    private val evaluationMatrix = arrayOfNulls<LocationEvaluation>(Board.ARRAY_SIZE)

    private val blackStats = OverallStats()
    private val whiteStats = OverallStats()

    private val moveGenerator = FastMoveGenerator(board)

    private var kingsEval = 0.0

    fun evaluate(): Double {
        var adder = 0.0
        var won = false
        if(board.hasWon(Color.WHITE)){
            adder = 10000.0
            won = true
        }
        if(board.hasWon(Color.BLACK)){
            adder = -10000.0
            won = true
        }
        prepare()
        board.eachLocation {
            evaluateLocation(it)
        }
        var evaluation = 0.0
        val movementEval = (whiteStats.movements.toDouble() / blackStats.movements) - (blackStats.movements.toDouble() / whiteStats.movements)
        val pieceValueEval = (whiteStats.pieceValue.toDouble() / blackStats.pieceValue) - (blackStats.pieceValue.toDouble() / whiteStats.pieceValue)
        kingsEval = if(!won) getKingsEval() else 0.0
        val matrixEval = evaluationMatrix.sumOf { it!!.evaluateField() }

        evaluation+=   5 * matrixEval
        evaluation+=   1 * movementEval
        evaluation+= 200 * pieceValueEval
        evaluation+=   3 * kingsEval

        /*println("White: " + whiteStats.movements)
        println("Black: " + blackStats.movements)

        println("movment: " + movementEval)
        println("piece: " + pieceValueEval)
        println("kings: " + kingsEval)
        println("matrix: " + matrixEval)*/

        return (evaluation / 10.0) + adder
    }

    fun evaluateBreakdown(): BoardEvaluationBreakdown {
        val total = evaluate()
        val movementEval = (whiteStats.movements.toDouble() / blackStats.movements) - (blackStats.movements.toDouble() / whiteStats.movements)
        val pieceValueEval = (whiteStats.pieceValue.toDouble() / blackStats.pieceValue) - (blackStats.pieceValue.toDouble() / whiteStats.pieceValue)
        val kingsEval = kingsEval
        val matrixEval = evaluationMatrix.sumOf { it!!.evaluateField() }
        return BoardEvaluationBreakdown(matrixEval, pieceValueEval, movementEval, kingsEval, total, evaluationMatrix)
    }

    private fun prepare(){
        for(i in evaluationMatrix.indices){
            evaluationMatrix[i] = LocationEvaluation()
        }
    }

    private fun evaluateLocation(location: Location){
        val eval = getAt(getIndexOfLocation(location))
        val piece = location.piece.value
        if(piece != null){
            val stats = getStats(piece.color)
            stats.pieceValue+= piece.type.value
            eval.occupiedBy = (piece.type.value * piece.color.evaluationNumber).toDouble()
            moveGenerator.getTargetLocations(piece).forEach {
                getAt(it).addThreat(piece.color, 1.0 / piece.type.value)
                if(moveGenerator.checkTarget(it.x, it.y, piece.color)){
                    //println(piece.type.getChar(piece.color) + piece.location.toString()  + "-" + it)
                    stats.movements++
                }
            }
        }
    }

    private fun getKingsEval(): Double {
        val whiteKing = board.findKing(Color.WHITE)!!
        val blackKing = board.findKing(Color.BLACK)!!

        val whiteEval = 20 * 2.0.pow(whiteKing.location.y - 6)
        val blackEval = 20 * 2.0.pow(-blackKing.location.y + 1)

        for(y in whiteKing.location.y until (Board.BOARD_SIZE - 1)){
            val field = getAt(Vector(whiteKing.location.x, y))
            field.weight+= whiteEval
        }
        for(y in blackKing.location.y downTo 1){
            val field = getAt(Vector(blackKing.location.x, y))
            field.weight+= blackEval
        }

        return (whiteEval / blackEval) - (blackEval / whiteEval)
    }

    private fun getIndexOfLocation(loc: Vector): Int {
        return (loc.y * Board.BOARD_SIZE + loc.x)
    }

    private fun getAt(vector: Vector): LocationEvaluation {
        return getAt(getIndexOfLocation(vector));
    }

    private fun getAt(index: Int): LocationEvaluation {
        return evaluationMatrix[index]!!
    }

    private fun getStats(color: Color): OverallStats {
        return if(color == Color.WHITE){
            whiteStats
        }else{
            blackStats
        }
    }

    class OverallStats(var movements: Int = 1, var pieceValue: Int = 1){

        override fun toString(): String {
            return "pieces=$pieceValue, moves=$movements"
        }
    }

    class LocationEvaluation(var occupiedBy: Double = 0.0,
                             var whiteControl: Double = 0.0,
                             var blackControl: Double = 0.0,
                             var weight: Double = 1.0){

        fun addThreat(color: Color, threat: Double){
            if(color == Color.WHITE){
                whiteControl+= threat
            }else{
                blackControl+= threat
            }
        }

        fun evaluateField(): Double {
            val whiteControl = this.whiteControl + 1
            val blackControl = this.blackControl + 1

            return when {
                occupiedBy > 0 -> {
                    ((1 + (whiteControl / blackControl) - (blackControl / whiteControl)) *
                            if(blackControl > whiteControl) occupiedBy else 1.0) * weight
                }
                occupiedBy < 0 -> {
                   ((-1 + (whiteControl / blackControl) - (blackControl / whiteControl)) *
                           if(whiteControl > blackControl) abs(occupiedBy) else 1.0) * weight
                }
                else -> {
                    ((whiteControl / blackControl) - (blackControl / whiteControl)) * weight
                }
            }
        }

    }

    fun printMatrix(){
        val d = evaluate()
        println("White: $whiteStats")
        println("Black: $blackStats")
        println("+-----------------+")
        for(y in 0 until Board.BOARD_SIZE){
            print("| ")
            for(x in 0 until Board.BOARD_SIZE){
                val s = (evaluationMatrix[y*8 + x]!!.evaluateField() * 2).toInt().toString()
                print(" ".repeat(max(0, 2-s.length)) + s)
            }
            println("|")
        }
        println("+-----------------+")
        println("Rating: $d")
    }

    class BoardEvaluationBreakdown(val matrix: Double,
                                   val pieces: Double,
                                   val movement: Double,
                                   val kings: Double,
                                   val total: Double,
                                   val evaluationMatrix: Array<LocationEvaluation?>)

}