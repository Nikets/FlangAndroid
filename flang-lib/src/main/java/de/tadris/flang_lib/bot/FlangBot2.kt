package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Type
import kotlin.math.max
import kotlin.math.min

class FlangBot2(val minDepth: Int, val maxDepth: Int) {

    var evaluations = 0
    var minMoveNumber = 0
    var bestMap: MutableMap<Int, MoveEvaluation> = mutableMapOf()

    fun findBestMove(board: Board) : BotResult {
        val start = System.currentTimeMillis()
        evaluations = 0
        minMoveNumber = 0
        bestMap.clear()
        val eval = findBestMove(board, maxDepth)
        val end = System.currentTimeMillis()
        println((end-start).toString() + "ms")
        return eval!!
    }

    private fun findBestMove(board: Board, depth: Int) : BotResult? {
        val moveEvaluations = mutableListOf<MoveEvaluation>()
        var bestEvaluation: MoveEvaluation? = null
        val allMoves = board.findAllMoves(board.atMove).toMutableList()
        allMoves.sortBy {
            return@sortBy when(it.piece.type){
                Type.KING -> -1
                else -> 0
            }
        }
        var alpha = -1000000.0
        var beta = 1000000.0
        for(it in allMoves) {
            val eval = MoveEvaluation(it, evaluateMove(board.executeOnNewBoard(it), depth-1, alpha, beta), depth)
            moveEvaluations+= eval

            if(bestEvaluation == null){
                bestEvaluation = eval
            }else{
                if(board.atMove == Color.WHITE){
                    if(eval.evaluation > bestEvaluation.evaluation){
                        bestEvaluation = eval
                    }
                    alpha = max(alpha, bestEvaluation.evaluation)
                    if(alpha >= beta){
                        println("Big alpha prune")
                        break
                    }
                }else{
                    if(eval.evaluation < bestEvaluation.evaluation){
                        bestEvaluation = eval
                    }
                    beta = min(beta, bestEvaluation.evaluation)
                    if(beta <= alpha){
                        println("Big beta prune")
                        break
                    }
                }
            }
        }

        moveEvaluations.shuffle()
        moveEvaluations.sortBy { -it.evaluation*board.atMove.evaluationNumber }
        if(moveEvaluations.size == 0){
            return null
        }
        val bestMove = moveEvaluations[0]

        return BotResult(MoveEvaluation(bestMove.move, bestMove.evaluation, depth), moveEvaluations, evaluations)
    }

    private fun evaluateMove(board: Board, depth: Int, alpha: Double, beta: Double) : Double {
        var alpha = alpha
        var beta = beta
        if(depth <= 0){
            evaluations++
            return BoardEvaluation(board).evaluate()
        }

        var bestEvaluation = -1000000.0*board.atMove.evaluationNumber


        var allMoves = board.findAllMoves(board.atMove)
        /*if(depth >= 4){
            allMoves = allMoves.sortedBy { -evaluateMove(board, depth-2, alpha, beta)*board.atMove.evaluationNumber }
                    .subList(0, min(allMoves.size, 5))
        }*/
        allMoves.forEach { move ->
            val hypotheticalBoard = board.executeOnNewBoard(move)
            val moveEvaluation = evaluateMove(hypotheticalBoard, depth - 1, alpha, beta)*0.99

            if(board.atMove == Color.WHITE){
                bestEvaluation = max(bestEvaluation, moveEvaluation)
                alpha = max(alpha, bestEvaluation)
                if(alpha >= beta){
                    return bestEvaluation
                }
            }else{
                bestEvaluation = min(bestEvaluation, moveEvaluation)
                beta = min(beta, bestEvaluation)
                if(beta <= alpha){
                    return bestEvaluation
                }
            }
        }

        return bestEvaluation
    }

    class BotResult(val bestMode: MoveEvaluation, val evaluations: List<MoveEvaluation>, val count: Int)

}