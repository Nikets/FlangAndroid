package de.tadris.flang_lib

enum class Color(val evaluationNumber: Int, val winningY: Int) {

    WHITE(1, Board.BOARD_SIZE-1),
    BLACK(-1, 0);

    fun getOpponent(): Color {
        return if(this == WHITE) BLACK else WHITE
    }

    companion object {

        fun getColor(char: Char): Color {
            return if(char.isUpperCase()) WHITE else BLACK
        }

    }

}