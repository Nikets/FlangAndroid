package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Type
import java.lang.IllegalArgumentException

class ActionList(board: Board? = null) {

    var board = board ?: Board(Board.DEFAULT_BOARD)

    val actionList = mutableListOf<Action>()

    fun loadMoveList(str: String): ActionList {
        str.split(" ").forEach {
            if(it.isEmpty()) return@forEach
            val action = parseAction(it)
            if(action != null){
                action.applyToBoard(board)
                actionList.add(action)
            }
        }
        return this
    }

    fun parseAction(actionStr: String): Action? {
        return if(isMove(actionStr)){
            Move.parse(board, actionStr)
        }else{
            when(actionStr[0]){
                '#' -> Resign(actionStr)
                else -> null
            }
        }
    }

    fun regenerateBoard(): Board {
        board = Board(Board.DEFAULT_BOARD)
        actionList.forEach {
            it.applyToBoard(board)
        }
        return board
    }

    fun isMove(actionStr: String): Boolean {
        return try{
            Type.getType(actionStr[0].toLowerCase())
            true
        }catch (ignored: IllegalArgumentException){
            false
        }
    }

    fun clone(): ActionList {
        val l = ActionList(board)
        l.actionList.addAll(actionList)
        return l
    }

    override fun toString(): String {
        return actionList.joinToString(separator = " ")
    }

}