package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color

class Resign(val color: Color) : Action {

    companion object {
        private const val WHITE_DRAW = '+'
        private const val BLACK_DRAW = '-'
    }

    constructor(str: String) : this(if(str[1] == WHITE_DRAW) Color.WHITE else Color.BLACK)

    override fun applyToBoard(board: Board) {
        board.resigned = color
        board.moveList.actionList.add(this)
    }

    override fun toString(): String {
        return "#" + if(color == Color.WHITE) WHITE_DRAW else BLACK_DRAW
    }
}