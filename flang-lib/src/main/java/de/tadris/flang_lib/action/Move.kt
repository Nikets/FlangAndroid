package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Piece
import de.tadris.flang_lib.Vector
import java.lang.IllegalArgumentException

class Move(val piece: Piece, val target: Location) : Action {

    fun getNotation(): String{
        return piece.getChar() + piece.location.getNotation() + "-" + target.getNotation()
    }

    override fun applyToBoard(board: Board) {
        board.executeOnBoard(this)
    }

    override fun toString(): String {
        return getNotation()
    }

    companion object {

        fun parse(board: Board, str: String): Move {
            val from = Vector.parse(str.substring(1, 3)).toLocation(board)
            val to = Vector.parse(str.substring(4, 6)).toLocation(board)
            if(!from.isValid()){
                throw IllegalArgumentException("Location $from is invalid")
            }
            if(!to.isValid()){
                throw IllegalArgumentException("Location $to is invalid")
            }
            val piece = from.piece.value ?: throw IllegalArgumentException("Cannot move null piece at $from on board '$board'")
            return Move(piece, to)
        }

    }
}