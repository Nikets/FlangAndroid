package de.tadris.flang.network_api.model

data class Games(val games: List<GameInfo>)
