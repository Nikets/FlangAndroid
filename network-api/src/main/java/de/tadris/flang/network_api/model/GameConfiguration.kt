package de.tadris.flang.network_api.model

data class GameConfiguration(val isRated: Boolean, val infiniteTime: Boolean, val time: Long, val ratingDiff: Int = 300, val isBotRequest: Boolean = false){

    companion object {
        const val CUSTOM_GAME_TIME = -3L
    }

    fun isCustomGame() = time == CUSTOM_GAME_TIME

}