package de.tadris.flang.network_api

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.tadris.flang.network_api.exception.*
import de.tadris.flang.network_api.model.*
import de.tadris.flang_lib.action.Move
import okhttp3.HttpUrl
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.lang.reflect.Type
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit


class FlangAPI @JvmOverloads constructor(
    private val host: String,
    private val port: Int,
    private val root: String,
    private val useSSL: Boolean,
    private val loggingEnabled: Boolean = false) {

    private val client: OkHttpClient
    private val gson = GsonBuilder().serializeNulls().create()

    fun getInfo(): ServerInfo {
        return getObject(
                object : TypeToken<ServerInfo>() {}.type, buildHttpUrl()
                .addPathSegment("info")
                .build()
        ) as ServerInfo
    }

    fun register(username: String, pwdHash: String) {
        fetchJsonString(
            buildHttpUrl()
                .addPathSegment("register")
                .addEncodedQueryParameter("username", username)
                .addEncodedQueryParameter("pwdHash", pwdHash)
                .build()
        )
    }

    fun newSession(username: String, pwdHash: String): Session {
        return getObject(
            object : TypeToken<Session>() {}.type, buildHttpUrl()
                .addPathSegment("newSession")
                .addEncodedQueryParameter("username", username)
                .addEncodedQueryParameter("pwdHash", pwdHash)
                .build()
        ) as Session
    }

    fun login(username: String, sessionKey: String) {
        fetchJsonString(
            buildHttpUrl()
                .addPathSegment("login")
                .addEncodedQueryParameter("username", username)
                .addEncodedQueryParameter("key", sessionKey)
                .build()
        )
    }

    fun requestGame(timeout: Long = 7000, gameConfiguration: GameConfiguration): GameRequestResult {
        return getObject(
            object : TypeToken<GameRequestResult>() {}.type, buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("request")
                .addPathSegment("add")
                .addEncodedQueryParameter("allowBots", gameConfiguration.isBotRequest.toString())
                .addEncodedQueryParameter("accept", true.toString())
                .addEncodedQueryParameter("timeout", timeout.toString())
                .addEncodedQueryParameter("isRated", gameConfiguration.isRated.toString())
                .addEncodedQueryParameter("infiniteTime", gameConfiguration.infiniteTime.toString())
                .addEncodedQueryParameter("time", gameConfiguration.time.toString())
                .addEncodedQueryParameter("range", gameConfiguration.ratingDiff.toString())
                .build()
        ) as GameRequestResult
    }

    fun acceptRequest(request: GameRequest): GameRequestResult{
        return getObject(
                object : TypeToken<GameRequestResult>() {}.type, buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("request")
                .addPathSegment("accept")
                .addPathSegment(request.id.toString())
                .build()
        ) as GameRequestResult
    }

    fun getLobby(): RequestLobby {
        return getObject(
                object : TypeToken<RequestLobby>() {}.type, buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("request")
                .addPathSegment("lobby")
                .build()
        ) as RequestLobby
    }

    fun getGameInfo(gameId: Long, moves: Int = -1, timeout: Long = 7000): GameInfo {
        return getObject(
            object : TypeToken<GameInfo>() {}.type, buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment(gameId.toString())
                .addEncodedQueryParameter("moves", moves.toString())
                .addEncodedQueryParameter("timeout", timeout.toString())
                .build()
        ) as GameInfo
    }

    fun executeMove(gameId: Long, move: Move) {
        fetchJsonString(
            buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("move")
                .addPathSegment(gameId.toString())
                .addEncodedQueryParameter("moveStr", move.getNotation())
                .build()
        )
    }

    fun resign(gameId: Long) {
        fetchJsonString(
            buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("resign")
                .addPathSegment(gameId.toString())
                .build()
        )
    }

    fun findUser(username: String): User {
        return getObject(
            object : TypeToken<User>() {}.type, buildHttpUrl()
                .addPathSegment("user")
                .addPathSegment(username)
                .build()
        ) as User
    }

    fun findGames(username: String, pageSize: Int, offset: Int): Games {
        return getObject(
                object : TypeToken<Games>() {}.type, buildHttpUrl()
                .addPathSegment("user")
                .addPathSegment(username)
                .addPathSegment("games")
                .addEncodedQueryParameter("max", pageSize.toString())
                .addEncodedQueryParameter("offset", offset.toString())
                .build()
        ) as Games
    }

    fun search(username: String): UserResult {
        return getObject(
                object : TypeToken<UserResult>() {}.type, buildHttpUrl()
                .addPathSegment("search")
                .addPathSegment(username)
                .build()
        ) as UserResult
    }

    fun getTopPlayers(): UserResult {
        return getObject(
                object : TypeToken<UserResult>() {}.type, buildHttpUrl()
                .addPathSegment("users")
                .addPathSegment("top")
                .build()
        ) as UserResult
    }

    fun getOnlinePlayers(): UserResult {
        return getObject(
            object : TypeToken<UserResult>() {}.type, buildHttpUrl()
                .addPathSegment("users")
                .addPathSegment("online")
                .build()
        ) as UserResult
    }

    fun tv(): FlangTvInfo {
        return getObject(
                object : TypeToken<FlangTvInfo>() {}.type, buildHttpUrl()
                .addPathSegment("tv")
                .build()
        ) as FlangTvInfo
    }

    fun findActive(): Games {
        return getObject(
            object : TypeToken<Games>() {}.type, buildHttpUrl()
                .addPathSegment("game")
                .addPathSegment("findActive")
                .build()
        ) as Games
    }

    fun findComputerResults(fmn: String): ComputerResults {
        return getObject(
                object : TypeToken<ComputerResults>() {}.type, buildHttpUrl()
                .addPathSegment("computer")
                .addPathSegment("results")
                .addEncodedQueryParameter("fmn", fmn)
                .build()
        ) as ComputerResults
    }

    fun queryOpeningDatabase(fmn: String): OpeningDatabaseResult {
        return getObject(
            object : TypeToken<OpeningDatabaseResult>() {}.type, buildHttpUrl()
                .addPathSegment("opening")
                .addPathSegment("query")
                .addEncodedQueryParameter("fmn", fmn)
                .build()
        ) as OpeningDatabaseResult
    }

    private fun getObject(type: Type, url: HttpUrl): Any {
        val jsonString = fetchJsonString(url)
        return gson.fromJson(jsonString, type)
    }

    private fun buildHttpUrl(): HttpUrl.Builder {
        return HttpUrl.Builder().scheme(if (useSSL) "https" else "http").host(host).port(port)
            .addPathSegment(
                root
            )
    }

    private fun fetchJsonString(url: HttpUrl): String {
        val request: Request = Request.Builder().url(url).build()
        return fetchJsonString(url, request)
    }

    private fun fetchJsonString(url: HttpUrl, request: Request): String {
        return try {
            val response = client.newCall(request).execute()
            val responseBody = response.body!!.string()
            if (loggingEnabled) {
                println(request.toString())
                println(response.toString())
                println(responseBody)
            }
            if (response.code >= 300) {
                when (response.code) {
                    400 -> throw BadRequestException("url: $url", responseBody)
                    401 -> throw UnauthorizedException(responseBody)
                    403 -> throw ForbiddenException("url: $url", responseBody)
                    404 -> throw NotFoundException("url: $url", responseBody)
                    500 -> throw ServerErrorException()
                    503 -> throw ServiceUnavailableException(responseBody)
                    else -> throw UnknownServerExpeption(response.code, responseBody)
                }
            }
            responseBody
        } catch (e: IOException) {
            throw ServerUnreachableException(e.message)
        }
    }

    fun destroyQuit() {
        try {
            destroy()
        } catch (ignored: IOException) {
        }
    }

    @Throws(IOException::class)
    fun destroy() {
        client.connectionPool.evictAll()
        client.dispatcher.executorService.shutdown()
        client.cache!!.close()
    }

    init {
        // init cookie manager
        val cookieHandler: CookieHandler = CookieManager(null, CookiePolicy.ACCEPT_ALL)
        // init OkHttpClient
        client = OkHttpClient.Builder()
            .cookieJar(JavaNetCookieJar(cookieHandler))
            .readTimeout(10, TimeUnit.SECONDS)
            .build()
    }
}