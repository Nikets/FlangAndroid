<resources>
    <string name="app_name">Flang</string>
    <string name="loading">Loading</string>
    <string name="username">Username</string>
    <string name="password">Password</string>
    <string name="actionLogin">Login</string>
    <string name="loggedIn">Logged in</string>
    <string name="loggedOut">Logged out</string>
    <string name="actionRegister">Register</string>
    <string name="offline">Offline</string>
    <string name="online">Online</string>
    <string name="onlineBot">Online Bot</string>
    <string name="actionResign">Resign</string>
    <string name="dialogShareTitle">Share board</string>
    <string name="dialogEnterNameMessage">Please enter a name for the current position:</string>
    <string name="actionCancel">Cancel</string>
    <string name="actionShare">Share</string>
    <string name="actionSwap">Swap sides</string>
    <string name="actionBack">Back</string>
    <string name="actionForward">Forward</string>
    <string name="analysisTitle">Analysis</string>
    <string name="computerHint">Hint</string>
    <string name="gameEndFlang">Flang!</string>
    <string name="gameEndTimeout">Time is up!</string>
    <string name="gameEndResign">Resigned!</string>
    <string name="gameEndUnknown">Game is over!</string>
    <string name="infiniteTimeChar" translatable="false">∞</string>
    <string name="menu_home">Home</string>
    <string name="modeRated">Rated</string>
    <string name="modeCasual">Casual</string>
    <string name="serverStatusMessage">%d players / %d games in play</string>
    <string name="requestingGame">Requesting game</string>
    <string name="waitingForOpponent">Waiting for opponent</string>
    <string name="menu_game">Game</string>
    <string name="gameRequestNotFound">Game request isn\'t available anymore</string>
    <string name="cancellingRequest">Cancelling request</string>
    <string name="offlineBotGame">Offline Bot Game</string>
    <string name="gameCross" translatable="false">⨯</string>
    <string name="gameInfoHeader" translatable="false">%s • %s • %s</string>
    <string name="gameInfoAge">%s ago</string>
    <string name="flangTv">Flang TV</string>
    <string name="tvNoGamesAvailable">Currently there are no games shown in TV, please wait</string>
    <string name="hintsEnabled">Hints enabled</string>
    <string name="hintsDisabled">Hints disabled</string>
    <string name="profileInfoString">
        Member since: %s\n
        Games: %d
    </string>
    <plurals name="minutesAgo">
        <item quantity="one">one minute ago</item>
        <item quantity="other">%d minutes ago</item>
    </plurals>
    <plurals name="hoursAgo">
        <item quantity="one">one hour ago</item>
        <item quantity="other">%d hours ago</item>
    </plurals>
    <plurals name="daysAgo">
        <item quantity="one">one day ago</item>
        <item quantity="other">%d days ago</item>
    </plurals>
    <plurals name="weeksAgo">
        <item quantity="one">one week ago</item>
        <item quantity="other">%d weeks ago</item>
    </plurals>
    <plurals name="monthsAgo">
        <item quantity="one">one month ago</item>
        <item quantity="other">%d months ago</item>
    </plurals>
    <plurals name="yearsAgo">
        <item quantity="one">one year ago</item>
        <item quantity="other">%d years ago</item>
    </plurals>
    <string name="momentsAgo">moments ago</string>
    <string name="gameResult" translatable="false">%s, %s</string>
    <string name="white">White</string>
    <string name="black">Black</string>
    <string name="isVictorious">%s is victorious</string>
    <string name="resultResigned">%s resigned</string>
    <string name="resultTimedOut">%s timed out</string>
    <string name="flang" translatable="false">Flang</string>
    <string name="gameRunning">Game is running</string>
    <string name="resultDraw">Draw</string>
    <string name="playerProfile">Player Profile</string>
    <string name="yourProfile">Your Profile</string>
    <string name="registerConsentPrivacyPolicy">I accept the privacy policy.</string>
    <string name="registerConsentAssistance">I agree that I will at no time receive assistance during my games (from a chess computer, book, database or another person).</string>
    <string name="registerConsentNice">I agree that I will always be nice to other players.</string>
    <string name="registerConsentMultipleAccounts">I agree that I will not create multiple accounts.</string>
    <string name="privacyPolicy">Privacy Policy</string>
    <string name="questionLoginTitle">Do you have an account</string>
    <string name="questionLoginMessage">Do you want to log into your existing account or register a new one?</string>
    <string name="tutorialTitle">Tutorial</string>
    <string name="tutorialQuestion">Do you want to learn the basic rules of flang?</string>
    <string name="okay">Okay</string>
    <string name="yes">Yes</string>
    <string name="no">No</string>
    <string name="tutorialIntroductionTitle">Introduction</string>
    <string name="tutorialIntroductionMessage">This tutorial aims to explain the basic rules of flang. What you see here is the default board setup. Click on the right arrow on the bottom to continue.</string>
    <string name="tutorialKingTitle">The King</string>
    <string name="tutorialKingMessage">Let\'s start by learning how the pieces move. The King can move in any direction but only one square.</string>
    <string name="tutorialPawnTitle">The Pawn</string>
    <string name="tutorialPawnMessage">The pawn can move only forward - straight and diagonal - and will be promoted to a Uni on the last row. Unlike chess, pawns can capture in any direction.</string>
    <string name="tutorialRookTitle">The Rook</string>
    <string name="tutorialRookMessage">The rook can move in vertical and horizontal direction.</string>
    <string name="tutorialHorseTitle">The Horse</string>
    <string name="tutorialHorseMessage">The horse (!) moves like the knight in chess: It can reach any of the closest squares that are not on the same rank, file, or diagonal. (l-Shape)</string>
    <string name="tutorialUniTitle">Uni</string>
    <string name="tutorialUniMessage">A Uni combines a chess-queen and a horse. It can move in vertical, horizontal and diagonal direction plus any move a horse can do.</string>
    <string name="tutorialFlangerTitle">The Flanger</string>
    <string name="tutorialFlangerMessage">A flanger can move in a zig-zag way horizontally and vertically. That means, that he can only be on white squares.</string>
    <string name="tutorialFreezeTitle">Freeze</string>
    <string name="tutorialFreezeMessage">The freeze-mechanic: You cannot move a piece twice because it is freezed in the next move. The king cannot be freezed.</string>
    <string name="tutorialWinBaseTitle">Winning - baseline</string>
    <string name="tutorialWinBaseMessage">The first option to win is by moving your king into the opponent\'s baseline.</string>
    <string name="tutorialWinCaptureTitle">Winning - capture the king</string>
    <string name="tutorialWinCaptureMessage">You also win when you capture the opponent\'s king.</string>
    <string name="tutorialCompleteTitle">Let\'s go</string>
    <string name="tutorialCompleteMessage">Now you know all the basic stuff you need to play a good game of flang. You can restart the tutorial at any time if you need it. Apart from that, I hope you have a lot fun playing Flang! ;)</string>
    <string name="tutorialTargetCapture">Try to capture the opponent\'s pieces</string>
    <string name="tutorialTargetReachLastRow">Try to reach the last row with your pawn</string>
    <string name="tutorialTargetWin">Try to win the game</string>
    <string name="actionLearn">Learn</string>
    <string name="tutorialNext">Next</string>
    <string name="topPlayers">Top Players</string>
    <string name="onlinePlayers">Online Players</string>
    <string name="serverOffline">The server is currently offline</string>
    <string name="customGameTitle">Custom Game</string>
    <string name="customGameTime">Time</string>
    <string name="customGameRatingDiff">Rating Difference</string>
    <string name="actionRequest">Request</string>
    <string name="playerInfo">Player Info</string>
    <string name="playerGames">Games</string>
    <string name="userSummary">Member since: %s\nGames: %d</string>
    <string name="rating">Rating</string>
    <string name="ratingHistory">Rating history</string>
    <string name="openingDatabaseEnabled">Opening database enabled</string>
    <string name="openingDatabaseDisabled">Opening database disabled</string>
</resources>