package de.tadris.flang.network

import android.content.Context
import de.tadris.flang.network_api.FlangAPI
import de.tadris.flang.network_api.exception.ForbiddenException
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GameRequest
import de.tadris.flang.network_api.model.GameRequestResult
import de.tadris.flang.util.Sha256
import java.lang.Exception
import java.util.*

const val HOST = "home.tadris.de"
const val PORT = 9090
const val ROOT = "flang"
const val SSL_ENABLED = true
const val DEBUG = false
const val LOGIN_INTERVAL = 15*60*1000

class DataProvider {

    companion object {

        private var instance: DataProvider? = null

        fun getInstance(): DataProvider {
            if(instance == null){
                instance = DataProvider()
            }
            return instance!!
        }
    }

    private var lastLogin = 0L
    val api = FlangAPI(HOST, PORT, ROOT, SSL_ENABLED, DEBUG)

    fun credentialsAvailable(context: Context) = CredentialsStorage(context).getUsername().isNotEmpty()

    fun login(context: Context, throwErrors: Boolean = false){
        synchronized(this){
            if(System.currentTimeMillis() - lastLogin > LOGIN_INTERVAL && credentialsAvailable(context)){
                try{
                    val credentialsStorage = CredentialsStorage(context)
                    api.login(credentialsStorage.getUsername(), credentialsStorage.getSessionKey())
                    lastLogin = System.currentTimeMillis()
                }catch (e: ForbiddenException){
                    e.printStackTrace()
                    if(throwErrors) throw e
                }
            }
        }
    }

    fun requestGame(context: Context, configuration: GameConfiguration): GameRequestResult {
        login(context)
        return api.requestGame(gameConfiguration = configuration)
    }

    fun acceptGame(context: Context, gameRequest: GameRequest): GameRequestResult {
        login(context)
        return api.acceptRequest(gameRequest)
    }

}