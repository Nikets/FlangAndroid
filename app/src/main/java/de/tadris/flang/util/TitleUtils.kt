package de.tadris.flang.util

import android.content.Context
import android.view.View
import android.widget.TextView
import de.tadris.flang.R
import de.tadris.flang.network_api.model.UserInfo

fun UserInfo.getTitleColor(context: Context): Int {

    return if(isBot){
        context.resources.getColor(R.color.botTitleColor)
    }else{
        context.resources.getColor(R.color.defaultTitleColor)
    }

}

fun UserInfo.applyTo(titleText: TextView, nameText: TextView, ratingText: TextView){
    nameText.text = username
    if(hasTitle()){
        titleText.visibility = View.VISIBLE
        titleText.text = getDisplayedTitle()
        titleText.setTextColor(getTitleColor(titleText.context))
    }else{
        titleText.visibility = View.GONE
    }
    ratingText.text = getRatingText()
}