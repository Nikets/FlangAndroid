package de.tadris.flang.util

import de.tadris.flang.network_api.model.GameConfiguration

object DefaultConfigurations {

    fun getConfigurations(): List<Pair<String, GameConfiguration>> {
        val map = mutableListOf<Pair<String, GameConfiguration>>()

        map+= Pair("Bullet", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 1))
        map+= Pair("Bullet", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 2))
        map+= Pair("Blitz", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 3))
        map+= Pair("Blitz", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 5))
        map+= Pair("Rapid", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 10))
        map+= Pair("Classical", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 30))
        map+= Pair("Bot Blitz", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 5, isBotRequest = true))
        map+= Pair("Bot Rapid", GameConfiguration(isRated = true, infiniteTime = false, time = 60000 * 15, isBotRequest = true))
        map+= Pair("Custom", GameConfiguration(isRated = true, infiniteTime = false, time = GameConfiguration.CUSTOM_GAME_TIME, isBotRequest = true))

        return map
    }

}