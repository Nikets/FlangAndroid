package de.tadris.flang.board

import android.animation.Animator
import android.animation.ValueAnimator
import de.tadris.flang_lib.*
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Typeface
import android.os.Handler
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.animation.*
import android.widget.ImageView

@SuppressLint("ClickableViewAccessibility")
class BoardView(private val rootView: ViewGroup, isClickable: Boolean, private var board: Board) {

    private val views = mutableListOf<FieldView>()
    private var isFlipped = false
    var listener: FieldClickListener? = null

    init {
        rootView.removeAllViews()
        if(isClickable){
            rootView.setOnTouchListener { _, event ->
                val location = unproject(event.x, event.y)
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    listener?.onFieldTouchBegin(location)
                } else if (event.actionMasked == MotionEvent.ACTION_UP) {
                    listener?.onFieldRelease(location)
                }
                true
            }
        }
    }

    fun setBoard(board: Board){
        detachAll()
        this.board = board
        board.eachLocation { loc ->
            val piece = loc.piece.value
            if(piece != null){
                attach(getPieceView(piece))
            }
        }
        board.printBoard()
    }

    fun refreshBoard(board: Board){
        this.board = board
        refresh()
    }

    fun refresh(){
        var isCapture = false
        var changes = 0

        val viewList = views.filterIsInstance<PieceView>().toMutableList()

        val oldViews = mutableListOf<PieceView>()
        val newPieces = mutableListOf<Piece>()

        board.eachLocation { loc ->
            val piece = loc.piece.value
            val view = viewList.find { it.getLocation() == loc }
            if(piece != null && view != null){
                if(piece != view.getPiece()){
                    // Piece changed
                    oldViews.add(view)
                    newPieces.add(piece)
                }else{
                    view.setPiece(piece)
                }
            }else if(piece != null && view == null){
                // Piece got here
                newPieces.add(piece)
            }else if(piece == null && view != null){
                // Piece moved away
                oldViews.add(view)
            }
        }

        newPieces.toList().forEach { piece ->
            val view = oldViews.find { it.getPiece() == piece }
            if(view != null){
                view.setPiece(piece)
                animateTo(view, piece.location)
                oldViews.remove(view)
            }else{
                attach(getPieceView(piece))
            }
            changes++
        }

        oldViews.forEach {
            detach(it)
            changes++
            isCapture = true
        }
    }

    fun showMessage(message: String, duration: Long){
        val text = AnnotationFieldView(rootView.context, Location(board, 0, 0), message)
        text.textSize = 48f
        text.typeface = Typeface.DEFAULT_BOLD
        text.setTextColor(Color.BLACK)

        rootView.addView(text, -1, ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        views.add(text)

        text.y = getViewSize() / 2f
        text.x = -getViewSize().toFloat()

        Handler().postDelayed({
            text.x = -text.width.toFloat()
            text.animate()
                    .setDuration(500)
                    .setInterpolator(OvershootInterpolator())
                    .translationX((getViewSize() - text.width) / 2f)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator?) {}
                        override fun onAnimationCancel(animation: Animator?) {}
                        override fun onAnimationRepeat(animation: Animator?) {}

                        override fun onAnimationEnd(animation: Animator?) {
                            text.clearAnimation()
                            text.animate()
                                    .setStartDelay(duration)
                                    .setDuration(1000)
                                    .setInterpolator(AccelerateInterpolator())
                                    .translationX(getViewSize() * 2f)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationStart(animation: Animator?) {}
                                        override fun onAnimationCancel(animation: Animator?) {}
                                        override fun onAnimationRepeat(animation: Animator?) {}

                                        override fun onAnimationEnd(animation: Animator?) {
                                            detach(text)
                                        }
                                    })
                                    .start()
                        }
                    })
                    .start()
        }, 200)
    }

    fun attach(fieldView: FieldView){
        if(fieldView.isFullOverlay()){
            rootView.addView(fieldView.getView(), -1, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        }else{
            rootView.addView(fieldView.getView(), if((fieldView is MiscView && !fieldView.type.foreground) || fieldView is AnnotationFieldView) 0 else -1, ViewGroup.LayoutParams(getScale().toInt(), getScale().toInt()))
            val vector = project(fieldView.getLocation())
            fieldView.getView().x = getScale() * vector.x
            fieldView.getView().y = getScale() * vector.y
        }
        views.add(fieldView)
    }

    fun animateTo(fieldView: ImageFieldView, location: Location){
        val vector = project(fieldView.getLocation())

        fieldView.clearAnimation()
        fieldView.animate()
            .setDuration(250)
            .setInterpolator(DecelerateInterpolator())
            .translationX(getScale() * vector.x)
            .translationY(getScale() * vector.y)
            .start()
    }

    fun project(vector: Vector): Vector {
        return if(!isFlipped){
            Vector(vector.x, Board.BOARD_SIZE - 1 - vector.y)
        }else{
            Vector(Board.BOARD_SIZE - 1 - vector.x, vector.y)
        }
    }

    fun unproject(x: Float, y: Float): Location {
        val fieldX = (x / getScale()).toInt()
        val fieldY = (y / getScale()).toInt()
        return if(isFlipped){
            Location(board, Board.BOARD_SIZE - 1 - fieldX, fieldY)
        }else{
            Location(board, fieldX, Board.BOARD_SIZE - 1 - fieldY)
        }
    }

    fun detachAllAnnotations(){
        views.filterIsInstance<AnnotationFieldView>().forEach {
            detach(it)
        }
    }

    fun detachAllArrows(){
        views.filterIsInstance<ArrowFieldView>().forEach {
            detach(it)
        }
    }

    fun detachAll(){
        rootView.removeAllViews()
        views.clear()
    }

    fun detach(fieldView: FieldView){
        rootView.removeView(fieldView.getView())
        views.remove(fieldView)
    }

    fun setFlipped(flipped: Boolean){
        isFlipped = flipped
        setBoard(board)
    }

    fun isFlipped(): Boolean {
        return isFlipped
    }

    fun getPieceView(piece: Piece): PieceView {
        return PieceView(rootView.context, piece)
    }

    fun getScale(): Float {
        return getViewSize().toFloat() / Board.BOARD_SIZE
    }

    fun getViewSize(): Int{
        return rootView.width
    }

    interface FieldClickListener {

        fun onFieldTouchBegin(location: Location)

        fun onFieldRelease(location: Location)

    }

}