package de.tadris.flang.board

import android.content.Context
import de.tadris.flang_lib.Location

abstract class ImageFieldView(context: Context) : androidx.appcompat.widget.AppCompatImageView(context), FieldView {

    override fun getView() = this

}