package de.tadris.flang.board

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.view.View
import de.tadris.flang_lib.action.Move
import kotlin.math.*

class ArrowFieldView(context: Context?, private val move: Move, private val boardView: BoardView, color: Int) : View(context), FieldView {

    private val scale = boardView.getScale()
    private val width = scale / 4

    private val posFromX: Float
    private val posFromY: Float
    private val posToX: Float
    private val posToY: Float

    init {
        val vectorFrom = boardView.project(move.piece.location)
        posFromX = vectorFrom.x * scale + (scale/2)
        posFromY = vectorFrom.y * scale + (scale/2)
        val vectorTo = boardView.project(move.target)
        posToX = vectorTo.x * scale + (scale/2)
        posToY = vectorTo.y * scale + (scale/2)
    }

    private val backgroundPaint = Paint()
    private val foregroundPaint = Paint()

    init {
        backgroundPaint.color = 0x00ffffff

        foregroundPaint.style = Paint.Style.STROKE
        foregroundPaint.strokeWidth = width
        foregroundPaint.color = color
    }

    override fun onDraw(canvas: Canvas?) {
        if(canvas == null) return

        //Paint Background
        canvas.drawRect(0f, 0f, width, height.toFloat(), backgroundPaint)

        //Draw Shaft
        val xDiff = posToX - posFromX
        val yDiff = posToY - posFromY
        val length = sqrt(xDiff.pow(2) + yDiff.pow(2))

        val xDiffNormal = xDiff / length
        val yDiffNormal = yDiff / length

        val xCutout = xDiffNormal * (width * 3 / 2)
        val yCutout = yDiffNormal * (width * 3 / 2)

        canvas.drawLine(posFromX, posFromY, posToX - xCutout, posToY - yCutout, foregroundPaint)

        val path = Path()
        path.fillType = Path.FillType.EVEN_ODD

        path.moveTo(posToX, posToY)
        path.lineTo(posToX + width, posToY + (width * 2 / 3))
        path.lineTo(posToX + width, posToY - (width * 2 / 3))
        path.close()

        // Something with this calculation is REALLY wrong!!!
        // Down below you can see how I tried to fix this.
        // It works but it looks so bad xD Good luck
        var angle = acos(yDiff.absoluteValue / length) * 360 / (2 * PI)

        if(xDiff > 0 || yDiff > 0){
            angle+= 180
        }
        if(xDiff < 0 && yDiff > 0){
            angle-= 90
        }
        if(xDiff > 0 && yDiff < 0){
            angle= 180-angle
        }

        canvas.rotate(90-angle.toFloat(), posToX, posToY)
        canvas.drawPath(path, foregroundPaint)
    }

    override fun isFullOverlay() = true

    override fun getLocation() = move.target

    override fun getView() = this
}