package de.tadris.flang.board

import android.content.Context
import androidx.annotation.DrawableRes
import de.tadris.flang.R
import de.tadris.flang_lib.*

class PieceView(context: Context, private var piece: Piece) : ImageFieldView(context) {

    init {
        setImageResource(getImageByPiece(piece))
        refresh()
    }

    override fun getLocation(): Location {
        return piece.location
    }

    fun getPiece(): Piece{
        return piece
    }

    fun setPiece(piece: Piece){
        this.piece = piece
        refresh()
    }

    fun refresh(){
        if(piece.state == PieceState.FROZEN){
            setBackgroundResource(R.color.frozen)
        }else{
            setBackgroundColor(0x0)
        }
    }

    companion object {

        @DrawableRes
        fun getImageByPiece(piece: Piece): Int{
            return when(piece.color){
                Color.BLACK -> when(piece.type){
                    Type.PAWN -> R.drawable.bp
                    Type.HORSE -> R.drawable.bn
                    Type.ROOK -> R.drawable.br
                    Type.UNI -> R.drawable.bq
                    Type.FLANGER -> R.drawable.bf
                    Type.KING -> R.drawable.bk
                }
                Color.WHITE -> when(piece.type){
                    Type.PAWN -> R.drawable.wp
                    Type.HORSE -> R.drawable.wn
                    Type.ROOK -> R.drawable.wr
                    Type.UNI -> R.drawable.wq
                    Type.FLANGER -> R.drawable.wf
                    Type.KING -> R.drawable.wk
                }
            }
        }
    }

}