package de.tadris.flang.board

import android.content.Context
import android.util.Log
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.Piece

class BoardMoveDetector(private val context: Context,
                        private val boardView: BoardView,
                        private val myColor: Color?,
                        var listener: MoveListener?) : BoardView.FieldClickListener {

    private var isAllowed = true

    var selected: MiscView? = null
    var options: MutableList<MiscView> = mutableListOf()

    override fun onFieldTouchBegin(location: Location) {
        Log.d("MoveDetector", location.toString())
        val piece = location.piece.value
        if(!isAllowed){
            return
        }
        if(selected == null){
            if(piece != null && piece.color == location.board.atMove && !location.board.gameIsComplete()){
                if(myColor == null || (location.board.atMove == myColor && myColor == piece.color)){
                    Log.d("MoveDetector", "Selecting $piece")
                    selectPiece(piece)
                }
            }
        }else{
            val clickedOnSelected = selected!!.getLocation() == location
            if(options.find { it.getLocation() == location } == null){
                deselect()
                if(piece != null && !clickedOnSelected){
                    onFieldTouchBegin(location)
                }
            }
        }
    }

    private fun selectPiece(piece: Piece){
        selected = MiscView(context, piece.location, MiscView.MiscType.SELECTED)
        boardView.attach(selected!!)
        options = mutableListOf()

        piece.getPossibleMoves().forEach { move ->
            addOption(move.target)
        }
    }

    private fun addOption(target: Location){
        val view = MiscView(context, target, MiscView.MiscType.OPTION)
        options.add(view)
        boardView.attach(view)
    }

    private fun deselect(){
        if(selected == null){ return }
        boardView.detach(selected!!)
        options.forEach {
            boardView.detach(it)
        }
        selected = null
        options.clear()
    }

    override fun onFieldRelease(location: Location) {
        options.toList().forEach {
            if(it.getLocation() == location){
                requestMove(Move(selected!!.getLocation().piece.value!!, location))
            }
        }
    }

    private fun requestMove(move: Move){
        deselect()
        listener?.onMoveRequested(move)
    }

    fun setAllowed(allowed: Boolean){
        this.isAllowed = allowed
        deselect()
    }

    interface MoveListener {
        fun onMoveRequested(move: Move)
    }

}