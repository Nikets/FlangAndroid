package de.tadris.flang.ui.view

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.tadris.flang.R
import de.tadris.flang.network_api.model.GameRequest
import de.tadris.flang.util.applyTo
import de.tadris.flang_lib.TimeUtils
import kotlin.math.roundToInt

class GameRequestView(val context: Activity, parent: ViewGroup, private val request: GameRequest, private val listener: GameRequestListener?){

    private val root = LayoutInflater.from(context).inflate(R.layout.view_game_request, parent, false)

    private val playerNameText = root.findViewById<TextView>(R.id.requestPlayerName)
    private val playerTitleText = root.findViewById<TextView>(R.id.requestPlayerTitle)
    private val playerRatingText = root.findViewById<TextView>(R.id.requestRating)
    private val timeText = root.findViewById<TextView>(R.id.requestTime)
    private val modeText = root.findViewById<TextView>(R.id.requestMode)

    init {
        request.requester.applyTo(playerTitleText, playerNameText, playerRatingText)
        if(request.configuration.infiniteTime){
            timeText.text = context.getString(R.string.infiniteTimeChar)
        }else{
            timeText.text = TimeUtils.getTimeAsString(request.configuration.time)
        }
        modeText.setText(if(request.configuration.isRated) R.string.modeRated else R.string.modeCasual)
        root.setOnClickListener { listener?.onClick(request) }
    }

    fun getView() = root

    interface GameRequestListener {

        fun onClick(gameRequest: GameRequest)

    }

}