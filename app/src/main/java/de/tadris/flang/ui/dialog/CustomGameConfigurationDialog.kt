package de.tadris.flang.ui.dialog

import android.app.Activity
import android.app.AlertDialog
import android.preference.PreferenceManager
import android.text.BoringLayout
import android.widget.ToggleButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import de.tadris.flang.R
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.exception.NotFoundException
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameRequest
import java.lang.Exception
import java.lang.Integer.max

class CustomGameConfigurationDialog(context: Activity, val listener: CustomGameConfigurationListener)  {

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    private val dialog = AlertDialog.Builder(context)
        .setTitle(R.string.customGameTitle)
        .setView(R.layout.dialog_custom_game_configuration)
        .setPositiveButton(R.string.actionRequest) { _, _ -> listener.onChoose(getConfiguration()) }
        .setNegativeButton(R.string.actionCancel, null)
        .setOnDismissListener { savePrefs() }
        .show()

    private val timeMap = mapOf(
        30 to R.id.chipTime30s,
        60 to R.id.chipTime1m,
        60*2 to R.id.chipTime2m,
        60*3 to R.id.chipTime3m,
        60*5 to R.id.chipTime5m,
        60*10 to R.id.chipTime10m,
        60*15 to R.id.chipTime15m,
        60*20 to R.id.chipTime20m,
        60*30 to R.id.chipTime30m,
        60*60 to R.id.chipTime1h,
        -1 to R.id.chipTimeInfinite,
    )

    private val ratingMap = mapOf(
        100 to R.id.chipRating100,
        200 to R.id.chipRating200,
        300 to R.id.chipRating300,
        400 to R.id.chipRating400,
        500 to R.id.chipRating500,
        -1 to R.id.chipRatingInfinite,
    )

    private val ratedButton: ToggleButton = dialog.findViewById(R.id.customGameRated)
    private val timeChips: ChipGroup = dialog.findViewById(R.id.timeChips)
    private val ratingChips: ChipGroup = dialog.findViewById(R.id.ratingChips)

    init {
        val time = prefs.getInt("customTime", 5*60_000)
        timeChips.check(timeMap[time / 1000] ?: R.id.chipTime5m)

        val ratingDiff = prefs.getInt("customRating", 300)
        ratingChips.check(ratingMap[ratingDiff] ?: R.id.chipRating300)

        ratedButton.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked && timeChips.checkedChipId == R.id.chipTimeInfinite){
                timeChips.check(R.id.chipTime5m)
            }
            refreshInfiniteTimeButton()
        }
        ratedButton.isChecked = prefs.getBoolean("customRated", true)
        refreshInfiniteTimeButton()
    }

    private fun refreshInfiniteTimeButton(){
        dialog.findViewById<Chip>(R.id.chipTimeInfinite).isEnabled = !ratedButton.isChecked
    }

    private fun savePrefs(){
        prefs.edit()
            .putInt("customTime", getTime())
            .putInt("customRating", getRatingDiff())
            .putBoolean("customRated", isRated())
            .apply()
    }

    private fun isRated() = ratedButton.isChecked

    private fun getTime() = (timeMap.entries.find { it.value == timeChips.checkedChipId }?.key ?: 5*60) * 1000

    private fun getRatingDiff() = ratingMap.entries.find { it.value == ratingChips.checkedChipId }?.key ?: 300

    private fun getConfiguration() = GameConfiguration(isRated(),
        getTime() == -1000,
        kotlin.math.max(0, getTime()).toLong(),
        getRatingDiff())

    interface CustomGameConfigurationListener {
        fun onChoose(configuration: GameConfiguration)
    }

}