package de.tadris.flang.ui.activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import de.tadris.flang.R
import kotlinx.coroutines.launch

class LoginActivity : AuthActivity() {

    lateinit var usernameText: TextView
    lateinit var passwordText: TextView
    lateinit var loginButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        usernameText = findViewById(R.id.loginUsername)
        passwordText = findViewById(R.id.loginPassword)
        loginButton = findViewById(R.id.loginSubmit)

        loginButton.setOnClickListener {
            lifecycleScope.launch {
                authenticate(usernameText.text.toString(), passwordText.text.toString(), false)
            }
        }

        AlertDialog.Builder(this)
                .setTitle(R.string.questionLoginTitle)
                .setMessage(R.string.questionLoginMessage)
                .setPositiveButton(R.string.actionRegister) { _: DialogInterface, _: Int -> startRegisterActivity() }
                .setNegativeButton(R.string.actionLogin, null)
                .setOnCancelListener { finish() }
                .show()

    }

    private fun startRegisterActivity(){
        startActivity(Intent(this, RegisterActivity::class.java))
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}