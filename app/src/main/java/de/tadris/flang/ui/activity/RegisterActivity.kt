package de.tadris.flang.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import de.tadris.flang.R
import kotlinx.coroutines.launch

class RegisterActivity : AuthActivity() {

    lateinit var usernameText: TextView
    lateinit var passwordText: TextView
    lateinit var registerButton: Button

    lateinit var consent1: CheckBox
    lateinit var consent2: CheckBox
    lateinit var consent3: CheckBox
    lateinit var consent4: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        usernameText = findViewById(R.id.registerUsername)
        passwordText = findViewById(R.id.registerPassword)
        registerButton = findViewById(R.id.registerSubmit)
        consent1 = findViewById(R.id.consent1)
        consent2 = findViewById(R.id.consent2)
        consent3 = findViewById(R.id.consent3)
        consent4 = findViewById(R.id.consent4)

        registerButton.setOnClickListener {
            lifecycleScope.launch {
                authenticate(usernameText.text.toString(), passwordText.text.toString(), true)
            }
        }

        consent1.setOnCheckedChangeListener { _, _ -> refresh() }
        consent2.setOnCheckedChangeListener { _, _ -> refresh() }
        consent3.setOnCheckedChangeListener { _, _ -> refresh() }
        consent4.setOnCheckedChangeListener { _, _ -> refresh() }

        findViewById<Button>(R.id.registerPrivacyPolicy).setOnClickListener {
            openPrivacyPolicy()
        }
    }

    private fun refresh(){
        registerButton.isEnabled = consent1.isChecked && consent2.isChecked && consent3.isChecked && consent4.isChecked
    }

    private fun openPrivacyPolicy(){
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacyPolicyUrl)))
        startActivity(browserIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}