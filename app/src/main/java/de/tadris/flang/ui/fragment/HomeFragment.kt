package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.WorkerThread
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tadris.flang.R
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.exception.NotFoundException
import de.tadris.flang.network_api.model.*
import de.tadris.flang.ui.activity.LoginActivity
import de.tadris.flang.ui.adapter.GameAdapter
import de.tadris.flang.ui.adapter.GameConfigurationAdapter
import de.tadris.flang.ui.adapter.ServerAnnouncementAdapter
import de.tadris.flang.ui.dialog.CustomGameConfigurationDialog
import de.tadris.flang.ui.dialog.GameRequestAcceptDialog
import de.tadris.flang.ui.dialog.GameRequestAddDialog
import de.tadris.flang.ui.dialog.GameRequestDialog
import de.tadris.flang.ui.view.GameRequestView
import de.tadris.flang.util.DefaultConfigurations
import kotlinx.coroutines.processNextEventInCurrentThread
import java.lang.Exception
import kotlin.concurrent.thread

class HomeFragment : Fragment(R.layout.fragment_home), GameConfigurationAdapter.ConfigurationListener, GameRequestView.GameRequestListener,
    GameAdapter.GameAdapterListener, CustomGameConfigurationDialog.CustomGameConfigurationListener {

    private lateinit var configurationRecyclerView: RecyclerView
    private lateinit var homeActiveGames: RecyclerView
    private lateinit var requestParent: ViewGroup
    private lateinit var infoText: TextView

    private val configurationAdapter = GameConfigurationAdapter(this)

    private lateinit var announcementsRecyclerView: RecyclerView
    private val announcementAdapter = ServerAnnouncementAdapter(emptyList())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!

        configurationRecyclerView = root.findViewById(R.id.requestConfigurations)
        homeActiveGames = root.findViewById(R.id.homeActiveGames)
        announcementsRecyclerView = root.findViewById(R.id.homeServerAnnouncements)
        requestParent = root.findViewById(R.id.requestsParent)
        infoText = root.findViewById(R.id.serverInfo)

        configurationRecyclerView.adapter = configurationAdapter
        configurationRecyclerView.layoutManager = GridLayoutManager(requireContext(), 3)

        announcementsRecyclerView.adapter = announcementAdapter
        announcementsRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        homeActiveGames.layoutManager = LinearLayoutManager(requireContext())

        root.findViewById<View>(R.id.homeLearn).setOnClickListener {
            openTutorial()
        }

        checkFirstStart()

        return root
    }

    override fun onResume() {
        super.onResume()
        startRefreshThread()
    }

    private fun startRefreshThread(){
        thread {
            try{
                DataProvider.getInstance().login(requireContext())
            }catch (e: Exception){
                e.printStackTrace()
            }
            while (isResumed){
                refreshLobby()
                Thread.sleep(4000)
            }
        }
    }

    @WorkerThread
    private fun refreshLobby(){
        try{
            val serverInfo = DataProvider.getInstance().api.getInfo()
            val lobby = DataProvider.getInstance().api.getLobby()
            val activeGames = if(DataProvider.getInstance().credentialsAvailable(requireContext())) DataProvider.getInstance().api.findActive() else null
            activity?.runOnUiThread {
                showInfo(serverInfo)
                showLobby(lobby)
                if(activeGames != null){
                    showActiveGames(activeGames)
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            activity?.runOnUiThread {
                infoText.text = getMessage(e)
            }
        }
    }

    private fun getMessage(e: Exception) = if(e is NotFoundException) getString(R.string.serverOffline) else e.message

    private fun showInfo(info: ServerInfo){
        infoText.text = getString(R.string.serverStatusMessage, info.playerCount, info.gameCount)
        announcementAdapter.updateList(info.announcements)
    }

    private fun showLobby(requestLobby: RequestLobby){
        requestParent.removeAllViews()
        requestLobby.requests.forEach { request: GameRequest ->
            val requestView = GameRequestView(requireActivity(), requestParent, request, this)
            requestParent.addView(requestView.getView())
        }
    }

    private fun showActiveGames(games: Games){
        homeActiveGames.adapter = GameAdapter(games.games.toMutableList(), this)
    }

    override fun onClick(configuration: GameConfiguration) {
        if(checkCredentials()){
            if(configuration.isCustomGame()){
                // User clicked on custom game
                CustomGameConfigurationDialog(requireActivity(), this)
            }else{
                GameRequestAddDialog(requireActivity(), configuration)
            }
        }
    }

    override fun onChoose(configuration: GameConfiguration) {
        onClick(configuration)
    }

    override fun onClick(gameRequest: GameRequest) {
        if(checkCredentials()){
            GameRequestAcceptDialog(requireActivity(), gameRequest)
        }
    }

    private fun checkCredentials(): Boolean {
        return if(DataProvider.getInstance().credentialsAvailable(requireContext())){
            true
        }else{
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            false
        }
    }

    private fun checkFirstStart() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if(prefs.getBoolean("firstStart", true)){
            prefs.edit().putBoolean("firstStart", false).apply()
            showTutorialDialog()
        }
    }

    private fun showTutorialDialog() {
        AlertDialog.Builder(activity)
                .setTitle(R.string.tutorialTitle)
                .setMessage(R.string.tutorialQuestion)
                .setPositiveButton(R.string.yes) { _: DialogInterface, _: Int -> openTutorial() }
                .setNegativeButton(R.string.no, null)
                .show()
    }

    private fun openTutorial() {
        findNavController().navigate(R.id.action_nav_home_to_nav_tutorial)
    }

    override fun onClick(gameInfo: GameInfo) {
        val bundle = Bundle()
        bundle.putLong(OnlineGameFragment.EXTRA_GAME_ID, gameInfo.gameId)
        findNavController().navigate(R.id.action_nav_home_to_nav_game, bundle)
    }

}