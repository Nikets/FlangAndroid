package de.tadris.flang.ui.dialog

import android.app.Activity
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.exception.NotFoundException
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameRequest
import java.lang.Exception

class GameRequestAddDialog(context: Activity, private val configuration: GameConfiguration) : GameRequestDialog(context, true) {

    override fun tryRequest() = DataProvider.getInstance().requestGame(context, configuration)

    override fun onError(e: Exception) = e is NotFoundException

}