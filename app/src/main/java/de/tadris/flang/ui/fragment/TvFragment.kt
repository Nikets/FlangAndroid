package de.tadris.flang.ui.fragment

import de.tadris.flang.R
import de.tadris.flang.game.FlangTvGameController
import de.tadris.flang.game.GameController
import de.tadris.flang.game.OfflineBotGameController
import de.tadris.flang.game.OnlineGameController

class TvFragment : GameFragment() {

    override fun createGameController(): GameController {
        return FlangTvGameController(requireActivity())
    }

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_tv_to_nav_analysis
}