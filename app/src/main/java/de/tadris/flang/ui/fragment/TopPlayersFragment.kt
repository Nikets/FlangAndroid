package de.tadris.flang.ui.fragment

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import de.tadris.flang.R
import de.tadris.flang.network.CredentialsStorage
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.UserInfo
import de.tadris.flang.ui.adapter.GameAdapter
import de.tadris.flang.ui.adapter.UserAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class TopPlayersFragment : Fragment(R.layout.fragment_top_players), UserAdapter.UserAdapterListener {

    private lateinit var topPlayersRecyclerView: RecyclerView
    private lateinit var onlinePlayersRecyclerView: RecyclerView

    private val topPlayersAdapter = UserAdapter(this)
    private val onlinePlayersAdapter = UserAdapter(this)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!

        topPlayersRecyclerView = root.findViewById(R.id.topPlayersRecyclerView)
        topPlayersRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        topPlayersRecyclerView.adapter = topPlayersAdapter

        onlinePlayersRecyclerView = root.findViewById(R.id.onlinePlayersRecyclerView)
        onlinePlayersRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        onlinePlayersRecyclerView.adapter = onlinePlayersAdapter

        return root
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    private fun refresh(){
        lifecycleScope.launch {
            try{
                topPlayersAdapter.updateList(findTopPlayers())
                onlinePlayersAdapter.updateList(findOnlinePlayers())
            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    @WorkerThread
    private suspend fun findTopPlayers() = withContext(Dispatchers.IO) {
        DataProvider.getInstance().api.getTopPlayers()
    }

    @WorkerThread
    private suspend fun findOnlinePlayers() = withContext(Dispatchers.IO) {
        DataProvider.getInstance().api.getOnlinePlayers()
    }

    override fun onClick(user: UserInfo) {
        showProfile(user.username)
    }

    private fun showProfile(username: String){
        val bundle = Bundle()
        bundle.putString(ProfileFragment.ARGUMENT_USERNAME, username)
        findNavController().navigate(R.id.action_nav_top_to_nav_profile, bundle)
    }

}