package de.tadris.flang.ui.activity

import android.os.Bundle
import android.view.Menu
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.WorkerThread
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import de.tadris.flang.R
import de.tadris.flang.network.CredentialsStorage
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.exception.ForbiddenException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var navView: NavigationView
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var headerUsernameText: TextView
    private lateinit var headerRatingText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_home, R.id.nav_profile, R.id.nav_offline_game, R.id.nav_tv, R.id.nav_top), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val headerView = navView.getHeaderView(0)

        headerUsernameText = headerView.findViewById(R.id.headerUsername)
        headerRatingText = headerView.findViewById(R.id.headerRating)
    }

    override fun onResume() {
        refreshHeader()
        super.onResume()

        navView.menu.findItem(R.id.nav_profile).isVisible = DataProvider.getInstance().credentialsAvailable(this)
    }

    private fun refreshHeader(){
        if(DataProvider.getInstance().credentialsAvailable(this)){
            lifecycleScope.launch {
                try {
                    try{
                        login()
                    }catch (e: ForbiddenException){
                        // User session isn't valid anymore
                        Toast.makeText(this@MainActivity, R.string.loggedOut, Toast.LENGTH_LONG).show()
                        CredentialsStorage(this@MainActivity).clear()
                        return@launch
                    }
                    val user = getUserInfo()
                    headerUsernameText.text = user.username
                    headerRatingText.text = user.getRatingText()
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
        }
    }

    @WorkerThread
    private suspend fun login() = withContext(Dispatchers.IO) {
        DataProvider.getInstance().login(this@MainActivity, true)
    }

    @WorkerThread
    private suspend fun getUserInfo() = withContext(Dispatchers.IO) {
        DataProvider.getInstance().api.findUser(CredentialsStorage(this@MainActivity).getUsername())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}