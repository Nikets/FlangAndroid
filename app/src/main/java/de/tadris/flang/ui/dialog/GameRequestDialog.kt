package de.tadris.flang.ui.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import de.tadris.flang.R
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.exception.NotFoundException
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameRequestResult
import de.tadris.flang.ui.fragment.OnlineGameFragment
import kotlin.concurrent.thread


abstract class GameRequestDialog(protected val context: Activity, private val isRepeating: Boolean = true) {

    private var requestRunning = true

    private val dialog = AlertDialog.Builder(context)
            .setTitle(R.string.requestingGame)
            .setMessage(R.string.waitingForOpponent)
            .setNegativeButton(R.string.actionCancel, null)
            .setCancelable(false)
            .show()

    init {
        if(isRepeating){
            val button: Button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE)
            button.setOnClickListener {
                button.isEnabled = false
                onCancelRequested()
            }
        }

        thread {
            var first = true
            while (requestRunning && (isRepeating || first)){
                first = false
                try{
                    val result = tryRequest()
                    requestRunning = false
                    context.runOnUiThread { onComplete(result) }
                }catch (e: Exception){
                    e.printStackTrace()
                    if(!onError(e)){
                        context.runOnUiThread {
                            Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                        }
                        requestRunning = false
                    }
                }
                Thread.sleep(500)
            }
            context.runOnUiThread { dialog.cancel() }
        }
    }

    protected abstract fun tryRequest(): GameRequestResult

    protected abstract fun onError(e: java.lang.Exception): Boolean

    private fun onCancelRequested(){
        dialog.setMessage(context.getString(R.string.cancellingRequest))
        requestRunning = false
    }

    private fun onComplete(result: GameRequestResult){
        val bundle = Bundle()
        bundle.putLong(OnlineGameFragment.EXTRA_GAME_ID, result.gameId)
        context.findNavController(R.id.nav_host_fragment).navigate(
            R.id.action_nav_home_to_nav_game,
            bundle
        )
    }

}