package de.tadris.flang.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tadris.flang.R
import de.tadris.flang.game.AnalysisGameController
import de.tadris.flang.game.GameController
import de.tadris.flang.game.OfflineBotGameController
import de.tadris.flang.game.OnlineGameController
import de.tadris.flang.network.CredentialsStorage
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.model.OpeningDatabaseEntry
import de.tadris.flang.ui.adapter.OpeningDatabaseEntriesAdapter
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class AnalysisGameFragment : GameFragment(),
    OpeningDatabaseEntriesAdapter.OpeningDatabaseEntryListener, GameFragment.BoardChangeListener {

    companion object {
        const val ARGUMENT_BOARD_FMN = "fmn"
        const val ARGUMENT_RUNNING_GAME = "running"
        const val ARGUMENT_FLIPPED = "flipped"
    }

    private lateinit var fmn: String
    private var running: Boolean = false
    private lateinit var firstBoard: Board
    private var isFlippedByDefault: Boolean = false

    private var openingDatabaseVisible = false
    private var openingDatabase: RecyclerView? = null
    private val openingDatabaseAdapter = OpeningDatabaseEntriesAdapter(this, emptyList())

    override fun onCreate(savedInstanceState: Bundle?) {
        fmn = arguments?.getString(ARGUMENT_BOARD_FMN) ?: ""
        running = arguments?.getBoolean(ARGUMENT_RUNNING_GAME) ?: false
        firstBoard = ActionList(Board(Board.DEFAULT_BOARD)).loadMoveList(fmn).board
        isFlippedByDefault = arguments?.getBoolean(ARGUMENT_FLIPPED) ?: false
        registerBoardChangeListener(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = super.onCreateView(inflater, container, savedInstanceState)
        analysisButton.visibility = View.GONE
        if(running){
            // Hide computer hints for running games
            hintButton.visibility = View.GONE
        }
        boardView.setFlipped(isFlippedByDefault)

        bookButton.visibility = View.VISIBLE
        bookButton.setOnClickListener { toggleOpeningDatabaseView() }

        v.findViewById<View>(R.id.player1InfoParent).visibility = View.GONE
        v.findViewById<View>(R.id.player2InfoParent).visibility = View.GONE
        v.findViewById<View>(R.id.player1TimeParent).visibility = View.GONE
        v.findViewById<View>(R.id.player2TimeParent).visibility = View.GONE

        resignButton.visibility = View.GONE // Hide resign button because it's useless
        return v
    }

    private fun toggleOpeningDatabaseView(){
        if(openingDatabaseVisible){
            removeOpeningDatabaseView()
        }else{
            addOpeningDatabaseView()
        }
    }

    private fun addOpeningDatabaseView(){
        if(openingDatabase == null){
            openingDatabase = RecyclerView(requireContext())
            openingDatabase!!.layoutManager = LinearLayoutManager(context)
            openingDatabase!!.adapter = openingDatabaseAdapter
        }
        additionalContentParent.addView(openingDatabase)
        openingDatabaseVisible = true
        refreshOpeningDatabase()
        Toast.makeText(requireContext(), R.string.openingDatabaseEnabled, Toast.LENGTH_SHORT).show()
    }

    private fun removeOpeningDatabaseView(){
        additionalContentParent.removeView(openingDatabase)
        openingDatabaseVisible = false
        Toast.makeText(requireContext(), R.string.openingDatabaseDisabled, Toast.LENGTH_SHORT).show()
    }

    override fun onEntryClick(entry: OpeningDatabaseEntry) {
        val move = Move.parse(displayedBoard, entry.move)
        onMoveRequested(move)
    }

    override fun onDisplayedBoardChange(board: Board) {
        if(openingDatabaseVisible){
            refreshOpeningDatabase()
        }
    }

    private fun refreshOpeningDatabase(){
            lifecycleScope.launch {
                try{
                    openingDatabaseAdapter.updateList(queryOpeningDatabase().result)
                }catch (e: Exception){
                    // TODO show error message
                    e.printStackTrace()
                }
            }
    }

    @WorkerThread
    private suspend fun queryOpeningDatabase() = withContext(Dispatchers.IO) {
        DataProvider.getInstance().api.queryOpeningDatabase(displayedBoard.getFMN())
    }

    override fun createGameController(): GameController {
        return AnalysisGameController(requireActivity(), firstBoard)
    }

    override fun getNavigationLinkToAnalysis() = 0
}