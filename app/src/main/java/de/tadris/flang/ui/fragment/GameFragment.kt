package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.tadris.flang.game.GameController
import de.tadris.flang.R
import de.tadris.flang.audio.AudioController
import de.tadris.flang.board.AnnotationFieldView
import de.tadris.flang.board.ArrowFieldView
import de.tadris.flang.board.BoardMoveDetector
import de.tadris.flang.board.BoardView
import de.tadris.flang.game.ComputerHints
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.ui.PlayerViewController
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Vector
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.BoardEvaluation
import kotlin.concurrent.thread
import kotlin.math.roundToInt

abstract class GameFragment : Fragment(R.layout.fragment_game),
    GameController.GameControllerCallback, BoardMoveDetector.MoveListener, ComputerHints.HintListener {

    lateinit var boardView: BoardView

    protected var gameBoard = Board(Board.DEFAULT_BOARD)
    protected var displayedBoard = Board(Board.DEFAULT_BOARD)

    private var isParticipant = false
    protected var isBoardDisabled = false

    private var color: Color? = null
    protected lateinit var additionalContentParent: ViewGroup
    protected lateinit var resignButton: ImageView
    protected lateinit var analysisButton: ImageView
    protected lateinit var shareButton: ImageView
    protected lateinit var backButton: ImageView
    protected lateinit var forwardButton: ImageView
    protected lateinit var swapSidesButton: ImageView
    protected lateinit var hintButton: ImageView
    protected lateinit var bookButton: ImageView
    protected lateinit var abstractButton: Button
    protected lateinit var spectatorRoot: ViewGroup
    protected lateinit var spectatorCount: TextView
    protected lateinit var player1ViewController: PlayerViewController
    protected lateinit var player2ViewController: PlayerViewController
    private var moveDetector: BoardMoveDetector? = null

    private var hintsEnabled = false
    private lateinit var hints: ComputerHints

    private lateinit var gameController: GameController

    protected var lastGameInfo: GameInfo? = null

    private val boardChangeListeners = mutableListOf<BoardChangeListener>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AudioController.getInstance(requireContext()) // Init sounds

        hints = ComputerHints(this)
        gameController = createGameController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!

        boardView = BoardView(root.findViewById(R.id.mainBoard), true, displayedBoard)

        resignButton = root.findViewById(R.id.resignButton)
        player1ViewController = PlayerViewController(Color.BLACK,
                root.findViewById(R.id.player1Name),
                root.findViewById(R.id.player1Title),
                root.findViewById(R.id.player1Rating),
                root.findViewById(R.id.player1RatingDiff),
                root.findViewById(R.id.player1TimeParent),
                root.findViewById(R.id.player1Time))
        player2ViewController = PlayerViewController(Color.BLACK,
                root.findViewById(R.id.player2Name),
                root.findViewById(R.id.player2Title),
                root.findViewById(R.id.player2Rating),
                root.findViewById(R.id.player2RatingDiff),
                root.findViewById(R.id.player2TimeParent),
                root.findViewById(R.id.player2Time))
        shareButton = root.findViewById(R.id.shareButton)
        backButton = root.findViewById(R.id.backButton)
        forwardButton = root.findViewById(R.id.forwardButton)
        analysisButton = root.findViewById(R.id.analysisButton)
        hintButton = root.findViewById(R.id.hintButton)
        swapSidesButton = root.findViewById(R.id.swapSidesButton)
        abstractButton = root.findViewById(R.id.abstractButton)
        spectatorRoot = root.findViewById(R.id.spectatorCountRoot)
        spectatorCount = root.findViewById(R.id.spectatorCount)
        bookButton = root.findViewById(R.id.openingDatabaseToggleButton)
        additionalContentParent = root.findViewById(R.id.gameAdditionalContentParent)

        resignButton.setOnClickListener {
            gameController.resignGame()
        }
        resignButton.visibility = View.GONE

        shareButton.setOnClickListener {
            showShareDialog()
        }
        analysisButton.setOnClickListener {
            openAnalysis()
        }
        backButton.setOnClickListener {
            back()
        }
        backButton.setOnLongClickListener {
            backToStart()
            true
        }
        forwardButton.setOnClickListener {
            forward()
        }
        forwardButton.setOnLongClickListener {
            setDisplayedBoardToGameBoard(true)
            true
        }
        hintButton.setOnClickListener {
            toggleComputerHints()
        }
        hintButton.visibility = if(gameController.isCreativeGame()) View.VISIBLE else View.GONE
        swapSidesButton.setOnClickListener {
            swapSides()
        }
        bookButton.visibility = View.GONE

        gameController.registerCallback(this)

        if(lastGameInfo == null){
            gameController.requestGame()
        }else{
            gameController.resume()
            onGameRequestSuccess(lastGameInfo!!, isParticipant, color)
        }

        Handler().postDelayed({
            if(context != null){
                reinitBoard()
            }
        }, 500)

        refreshBoardView()

        return root
    }

    override fun onDestroyView() {
        gameController.stop()
        super.onDestroyView()
    }

    override fun onResume() {
        runClockThread()
        super.onResume()
    }

    private fun runClockThread(){
        thread {
            while (isResumed){
                activity?.runOnUiThread {
                    updateClocks()
                }
                Thread.sleep(500)
            }
        }
    }

    private fun updatePlayerInfo(){
        player1ViewController.color = if(boardView.isFlipped()) Color.WHITE else Color.BLACK
        player2ViewController.color = player1ViewController.color.getOpponent()

        if(lastGameInfo != null){
            player1ViewController.update(lastGameInfo!!)
            player2ViewController.update(lastGameInfo!!)

            // Viewers that are not players
            val gameRunning = lastGameInfo!!.running || lastGameInfo!!.lastAction > System.currentTimeMillis() - 30_000
            val realSpectators = lastGameInfo!!.spectatorCount - (if(gameRunning) 2 else 0)
            // Minimum viewer count to display the count
            val minSpectators = if(gameRunning) 1 else 2
            spectatorRoot.visibility = if(realSpectators >= minSpectators) View.VISIBLE else View.INVISIBLE
            spectatorCount.text = realSpectators.toString()
        }

        updateClocks()
    }

    private fun updateClocks(){
        if(lastGameInfo != null){
            player1ViewController.updateClock(lastGameInfo!!.configuration.infiniteTime, lastGameInfo!!.running && gameBoard.atMove == player1ViewController.color)
            player2ViewController.updateClock(lastGameInfo!!.configuration.infiniteTime, lastGameInfo!!.running && gameBoard.atMove == player2ViewController.color)
        }
    }

    override fun onGameRequestSuccess(info: GameInfo, isParticipant: Boolean, color: Color?, board: Board?) {
        this.gameBoard = board ?: info.toBoard()
        this.lastGameInfo = info
        this.color = color
        this.isParticipant = isParticipant
        boardView.setFlipped(color == Color.BLACK)
        if(isParticipant && info.running){
            moveDetector = BoardMoveDetector(requireContext(), boardView, color, this)
            boardView.listener = moveDetector
            resignButton.visibility = View.VISIBLE
            AudioController.getInstance(requireContext()).playSound(AudioController.SOUND_NOTIFY_GENERIC)
        }else{
            resignButton.visibility = View.GONE
        }
        updatePlayerInfo()
        setDisplayedBoardToGameBoard(true)
    }

    override fun onGameRequestFailed(reason: String) {
        Toast.makeText(requireContext(), reason, Toast.LENGTH_LONG).show()
    }

    override fun onMoveRequested(move: Move) {
        var boardRequest: Board? = null
        if(!isDisplayedBoardGameBoard()){
            if(gameController.isCreativeGame()){
                gameBoard = displayedBoard.clone(true)
                boardRequest = gameBoard.clone(true)
            }else{
                return
            }
        }
        moveDetector?.setAllowed(false)
        gameController.onMoveRequested(move, boardRequest)
    }

    override fun onUpdate(action: Action) {
        val isOverBefore = gameBoard.gameIsComplete()
        val isCapture = action is Move && action.target.onDifferentBoard(gameBoard).piece.value != null

        action.applyToBoard(gameBoard)

        val isOverAfter = gameBoard.gameIsComplete()

        if(context != null){
            if(isOverAfter && !isOverBefore){
                AudioController.getInstance(requireContext()).playSound(AudioController.SOUND_NOTIFY_GENERIC)
                @StringRes var message = R.string.gameEndUnknown
                if(lastGameInfo != null && !lastGameInfo!!.configuration.infiniteTime && (lastGameInfo!!.white.time < 0 || lastGameInfo!!.black.time < 0)){
                    message = R.string.gameEndTimeout
                }else if(gameBoard.resigned != null){
                    message = R.string.gameEndResign
                }else if(gameBoard.hasWon(Color.WHITE) || gameBoard.hasWon(Color.BLACK)){
                    message = R.string.gameEndFlang
                }
                boardView.showMessage(getString(message), 2000)
            }else if(action is Move){
                AudioController.getInstance(requireContext()).playSound(
                    if(isCapture){ AudioController.SOUND_MOVE_CAPTURE } else { AudioController.SOUND_MOVE }
                )
            }
        }
        setDisplayedBoardToGameBoard(true)

        if(gameBoard.gameIsComplete()){
            resignButton.visibility = View.GONE
        }

        updateClocks()
    }

    override fun onUpdate(gameInfo: GameInfo) {
        this.lastGameInfo = gameInfo
        updatePlayerInfo()
    }

    private fun setDisplayedBoardToGameBoard(force: Boolean){
        if(!force && !isDisplayedBoardGameBoard()){ return }
        displayedBoard = gameBoard.clone(true)
        refreshBoardView()
    }

    protected fun refreshBoardView(){
        boardView.refreshBoard(displayedBoard)
        moveDetector?.setAllowed(movesAllowed())
        backButton.isEnabled = displayedBoard.moveList.actionList.size > 0
        forwardButton.isEnabled = !isDisplayedBoardGameBoard()
        requestHintsIfEnabled()
        boardChangeListeners.forEach {
            it.onDisplayedBoardChange(displayedBoard)
        }
    }

    private fun movesAllowed(): Boolean {
        return when {
            isBoardDisabled -> {
                false
            }
            gameController.isCreativeGame() -> {
                true
            }
            else -> {
                isDisplayedBoardGameBoard() && !gameBoard.gameIsComplete() && (color == null || color == gameBoard.atMove)
            }
        }
    }

    private fun backToStart(){
        displayedBoard = Board(Board.DEFAULT_BOARD)
        refreshBoardView()
    }

    private fun back(){
        val moveList = displayedBoard.moveList
        println("undoing: " + moveList.actionList.removeLast())
        displayedBoard = moveList.regenerateBoard()
        refreshBoardView()
    }

    private fun forward(){
        val index = displayedBoard.moveList.actionList.size
        val action = gameBoard.moveList.actionList[index]
        println("redoing: $action")
        action.applyToBoard(displayedBoard)
        refreshBoardView()
    }

    private fun isDisplayedBoardGameBoard(): Boolean{
        return displayedBoard.moveNumber == gameBoard.moveNumber
    }

    private fun refreshBreakdown(){
        val eval = BoardEvaluation(displayedBoard)
        val breakdown = eval.evaluateBreakdown()
        /*evaluationText.text = "Pieces: ${breakdown.pieces}\n" +
                "Matrix: ${breakdown.matrix}\n" +
                "Kings: ${breakdown.kings}\n" +
                "Movement: ${breakdown.movement}\n" +
                "Total: ${breakdown.total}\n"*/
        boardView.detachAllAnnotations()

        for(i in breakdown.evaluationMatrix.indices){
            val evaluation = breakdown.evaluationMatrix[i]!!
            val location = Vector(i % Board.BOARD_SIZE, i / Board.BOARD_SIZE).toLocation(displayedBoard)
            val annotation = AnnotationFieldView(requireContext(), location, (evaluation.evaluateField()).roundToInt().toString())
            annotation.setTextColor(0xaa000000.toInt().or(0x0f * evaluation.weight.toInt()))
            boardView.attach(annotation)
        }

    }

    private fun openAnalysis(){
        val bundle = Bundle()
        bundle.putString(AnalysisGameFragment.ARGUMENT_BOARD_FMN, displayedBoard.getFMN())
        bundle.putBoolean(AnalysisGameFragment.ARGUMENT_RUNNING_GAME, !gameBoard.gameIsComplete() && !gameController.isCreativeGame())
        bundle.putBoolean(AnalysisGameFragment.ARGUMENT_FLIPPED, boardView.isFlipped())
        findNavController().navigate(getNavigationLinkToAnalysis(), bundle)
    }

    @IdRes
    protected abstract fun getNavigationLinkToAnalysis(): Int

    private fun swapSides(){
        boardView.setFlipped(!boardView.isFlipped())
        reinitBoard()
        updatePlayerInfo()
    }

    private fun reinitBoard(){
        boardView.setBoard(displayedBoard)
        refreshBoardView()
        val charsY = if(boardView.isFlipped()) Board.BOARD_SIZE-1 else 0
        val numbersX = if(boardView.isFlipped()) 0 else Board.BOARD_SIZE-1
        for(x in 0 until Board.BOARD_SIZE){
            val location = Location(displayedBoard, x, charsY)
            val view = AnnotationFieldView(requireContext(), location, ('A'.toInt() + x).toChar().toString())
            view.setTextColor(resources.getColor(if((x % 2 == 1).xor(boardView.isFlipped())) R.color.boardBlack else R.color.boardWhite))
            view.textSize = 12f
            boardView.attach(view)
        }
        val minY = if(boardView.isFlipped()) 0 else 1
        val maxY = Board.BOARD_SIZE - if(boardView.isFlipped()) 1 else 0
        for(y in minY until maxY){
            val location = Location(displayedBoard, numbersX, y)
            val view = AnnotationFieldView(requireContext(), location, (y + 1).toString())
            view.setTextColor(resources.getColor(if((y % 2 == 0).xor(boardView.isFlipped())) R.color.boardBlack else R.color.boardWhite))
            view.textSize = 12f
            boardView.attach(view)
        }
    }

    private fun toggleComputerHints(){
        clearHints()
        hintsEnabled = !hintsEnabled
        Toast.makeText(requireContext(), if(hintsEnabled) R.string.hintsEnabled else R.string.hintsDisabled, Toast.LENGTH_SHORT).show()
        requestHintsIfEnabled()
    }

    private fun requestHintsIfEnabled(){
        if(hintsEnabled){
            requestHints()
        }
    }

    private fun requestHints(){
        hints.requestHints(displayedBoard.clone(true))
        clearHints()
    }

    private fun clearHints(){
        boardView.detachAllArrows()
    }

    override fun onHintsResult(hints: List<ComputerHints.ComputerHint>) {
        activity?.runOnUiThread {
            hints.forEach {
                boardView.attach(ArrowFieldView(context, it.move, boardView, it.color))
            }
        }
    }

    private fun showShareDialog(){
        val editText = EditText(requireContext())
        AlertDialog.Builder(requireContext())
                .setTitle(R.string.dialogShareTitle)
                .setMessage(R.string.dialogEnterNameMessage)
                .setView(editText)
                .setPositiveButton(R.string.actionShare) { _, _ ->
                    shareBoard(editText.text.toString())
                }
                .setNegativeButton(R.string.actionCancel, null)
                .show()
    }

    private fun shareBoard(name: String){
        val text = "$name\n\n${displayedBoard.getFMN()}\n\n${displayedBoard.getFBN()}"

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(intent)
    }

    abstract fun createGameController(): GameController

    fun registerBoardChangeListener(listener: BoardChangeListener){
        boardChangeListeners.add(listener)
    }

    interface BoardChangeListener {

        fun onDisplayedBoardChange(board: Board)

    }

}