package de.tadris.flang.ui.fragment

import de.tadris.flang.R
import de.tadris.flang.game.GameController
import de.tadris.flang.game.OfflineBotGameController
import de.tadris.flang.game.OnlineGameController

class OfflineGameFragment : GameFragment() {

    override fun createGameController(): GameController {
        return OfflineBotGameController(requireActivity())
    }

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_offline_game_to_nav_analysis
}