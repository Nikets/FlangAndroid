package de.tadris.flang.game

import android.app.Activity
import android.util.Log
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.action.Resign

class AnalysisGameController(activity: Activity, var board: Board) : AbstractGameController(activity) {

    override fun onMoveRequested(move: Move, newBoardRequest: Board?) {
        if(newBoardRequest != null){
            board = newBoardRequest
        }
        board.executeOnBoard(move)
        callback.onUpdate(move)
        Log.d("Analysis", board.getFBN())
        Log.d("Analysis", board.getFMN())
    }

    override fun requestGame() {
        val player1Info = GamePlayerInfo("Player1", 0f, -1, 0f, false, "")
        val player2Info = GamePlayerInfo("Player2", 0f, -1, 0f, false, "")
        callback.onGameRequestSuccess(GameInfo(-1, player1Info, player2Info, board.getFMN(), board.moveList.actionList.size, running = true, GameConfiguration(
            isRated = false,
            infiniteTime = true,
            time = 0,
            isBotRequest = false
        ), lastAction = 0, won = 0, spectatorCount = 0), true, null)
    }

    override fun resignGame() {
        callback.onUpdate(Resign(board.atMove))
    }

    override fun isCreativeGame() = true
}