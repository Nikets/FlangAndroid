package de.tadris.flang.game

import androidx.annotation.WorkerThread
import de.tadris.flang.network.DataProvider
import de.tadris.flang.network_api.model.ComputerResult
import de.tadris.flang.network_api.model.ComputerResults
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.FlangBot
import de.tadris.flang_lib.bot.MoveEvaluation
import java.lang.Exception
import kotlin.concurrent.thread
import kotlin.math.absoluteValue
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

class ComputerHints(private val listener: HintListener) {
    
    var count = 0

    fun requestHints(board: Board){
        val oldCount = ++count
        thread {
            var hints = getHints(board, safeRequestResults(board, OnlineHintSource()))
            if(hints.isEmpty() && oldCount == count){
                hints = getHints(board, safeRequestResults(board, OfflineHintSource()))
            }
            if(hints.isNotEmpty() && oldCount == count){
                listener.onHintsResult(hints)
            }
        }
    }

    private fun safeRequestResults(board: Board, hintSource: ComputerHintSource): ComputerResults {
        return try{
            hintSource.findHints(board)
        }catch(e: Exception){
            e.printStackTrace()
            ComputerResults(emptyList())
        }
    }

    private fun getHints(board: Board, results: ComputerResults): List<ComputerHint> {
        val bestResultsPerAlgorithm = results.results.groupBy { it.name }.values
                .mapNotNull { result -> result.maxByOrNull { it.depth } }
        return bestResultsPerAlgorithm.map { getHint(board, it) }.flatten()
    }
    
    private fun getHint(board: Board, result: ComputerResult): List<ComputerHint> {
        val name = result.name + result.depth
        val moves = result.getMoveEvals(board).sortedByDescending { it.evaluation * board.atMove.evaluationNumber }
        return if(moves.isNotEmpty()){
            val goodMoves = moves.filter { if(board.atMove == Color.WHITE) it.evaluation > -100 else it.evaluation < 100 }
            if(goodMoves.isNotEmpty()){
                val sumEval = goodMoves.sumByDouble { it.evaluation }
                val avgEval = sumEval / goodMoves.size
                val variance2 = goodMoves.sumByDouble { (it.evaluation - avgEval).pow(2) }
                val variance = sqrt(variance2)
                val maxDiff = variance * 0.15
                val bestEval = goodMoves.first().evaluation
                goodMoves.subList(0, min(5, goodMoves.size)).filter { (bestEval - it.evaluation).absoluteValue < maxDiff }
                        .map { getHintFromEval(name, it, ((1 - (bestEval - it.evaluation).absoluteValue / maxDiff).pow(5)*255).toInt()) }
            }else{
                listOf(getHintFromEval(name, moves.first(), 255))
            }
        }else{
            emptyList()
        }
    }

    private fun getHintFromEval(name: String, moveEvaluation: MoveEvaluation, alpha: Int) = ComputerHint(moveEvaluation.move, name.hashCode().and(0x00ffffff).or(alpha.shl(24)))
    
    data class ComputerHint(val move: Move, val color: Int)

    interface ComputerHintSource {

        @WorkerThread
        fun findHints(board: Board): ComputerResults

    }

    class OfflineHintSource : ComputerHintSource {

        override fun findHints(board: Board): ComputerResults {
            val bot = FlangBot(OfflineBotGameController.STRENGTH, OfflineBotGameController.STRENGTH)
            val botMoves = bot.findBestMove(board)
            val result = botMoves.evaluations.joinToString(separator = ";") { it.getNotation() }
            return ComputerResults(listOf(ComputerResult(result, "OFF", OfflineBotGameController.STRENGTH)))
        }
    }

    class OnlineHintSource : ComputerHintSource {

        override fun findHints(board: Board) = DataProvider.getInstance().api.findComputerResults(board.getFMN())
        
    }

    interface HintListener {

        @WorkerThread
        fun onHintsResult(hints: List<ComputerHint>)

    }

}