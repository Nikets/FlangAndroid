package de.tadris.flang.game

import android.app.Activity
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move

class TutorialGameController(private val tutorial: TutorialInfo, activity: Activity) : OfflineBotGameController(activity) {

    override fun onMoveRequested(move: Move, newBoardRequest: Board?) {
        if(tutorial.botTurns){
            super.onMoveRequested(move, newBoardRequest)
        }else{
            board.executeOnBoard(move)
            callback.onUpdate(move)
            board.atMove = Color.WHITE
        }
        if(!tutorial.freezeEnabled){
            board.unfreezeAllOnBoard(Color.WHITE)
            board.unfreezeAllOnBoard(Color.BLACK)
        }
    }

    override fun requestGame() {
        board = tutorial.toBoard()
        board.isInfiniteGame = !tutorial.botTurns
        callback.onGameRequestSuccess(GameInfo(-1,
                GamePlayerInfo("Player1", 0f, -1, 0f, false, ""),
                GamePlayerInfo("Flangbot Classic#$STRENGTH", 0f, -1, 0f, false, ""),
                "", 0, running = true, GameConfiguration(
                isRated = false,
                infiniteTime = true,
                time = 0,
                isBotRequest = false
        ), lastAction = 0, won = 0, spectatorCount = 0), true, color, board.clone(true))
    }

    override fun isCreativeGame() = true
}