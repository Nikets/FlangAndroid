package de.tadris.flang.game

import androidx.annotation.StringRes
import de.tadris.flang.R
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Color.*
import de.tadris.flang_lib.Type

enum class TutorialInfo(val index: Int,
                        @StringRes val title: Int,
                        @StringRes val description: Int,
                        val boardData: String,
                        val freezeEnabled: Boolean = true,
                        val clickable: Boolean = true,
                        val hintsEnabled: Boolean = false,
                        val finish: Boolean = false,
                        val botTurns: Boolean = true,
                        val target: Target = Target.NONE) {

    INTRODUCTION(0, R.string.tutorialIntroductionTitle, R.string.tutorialIntroductionMessage,
            Board.DEFAULT_BOARD,
            clickable = false),
    KING(1, R.string.tutorialKingTitle, R.string.tutorialKingMessage,
            "        " +
            "  K     " +
            "        " +
            "        " +
            "        " +
            "    p   " +
            "        " +
            "        ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.CAPTURE),
    PAWN(2, R.string.tutorialPawnTitle, R.string.tutorialPawnMessage,
            "        " +
            "  P     " +
            "        " +
            "        " +
            "        " +
            "        " +
            "        " +
            "        ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.PROMOTE_A_UNI),
    ROOK(3, R.string.tutorialRookTitle, R.string.tutorialRookMessage,
            "        " +
                    "      R " +
                    "        " +
                    "        " +
                    "  p     " +
                    "        " +
                    "        " +
                    "        ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.CAPTURE),
    HORSE(4, R.string.tutorialHorseTitle, R.string.tutorialHorseMessage,
            "        " +
                    "      H " +
                    "        " +
                    "        " +
                    "   p    " +
                    "        " +
                    "        " +
                    "        ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.CAPTURE),
    UNI(5, R.string.tutorialUniTitle, R.string.tutorialUniMessage,
            "        " +
                    " p    U " +
                    "        " +
                    "        " +
                    "   p  p " +
                    "        " +
                    "        " +
                    "   p    ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.CAPTURE),
    FLANGER(6, R.string.tutorialFlangerTitle, R.string.tutorialFlangerMessage,
            "        " +
                    "      F " +
                    "        " +
                    "      p " +
                    "        " +
                    "p       " +
                    "     p  " +
                    "        ",
            freezeEnabled = false,
            botTurns = false,
            target = Target.CAPTURE),
    FREEZE(7, R.string.tutorialFreezeTitle, R.string.tutorialFreezeMessage,
            "        " +
                    "  PPP F " +
                    "        " +
                    "      p " +
                    "        " +
                    "p       " +
                    "     p  " +
                    "        ",
            target = Target.CAPTURE,
            botTurns = false),
    WIN_BASE(8, R.string.tutorialWinBaseTitle, R.string.tutorialWinBaseMessage,
            "        " +
                    " KPPP   " +
                    "        " +
                    "        " +
                    "        " +
                    "        " +
                    "        " +
                    "        ",
            target = Target.WIN_BASELINE,
            botTurns = false),
    WIN_CAPTURE(9, R.string.tutorialWinCaptureTitle, R.string.tutorialWinCaptureMessage,
            " U      " +
                    "  PPP   " +
                    "        " +
                    "   k    " +
                    "        " +
                    "        " +
                    "        " +
                    "        ",
            target = Target.WIN_CAPTURE,
            botTurns = false),
    COMPLETE(10, R.string.tutorialCompleteTitle, R.string.tutorialCompleteMessage,
            Board.DEFAULT_BOARD,
            target = Target.NONE, finish = true, hintsEnabled = true),



    ;

    fun toBoard() = Board(boardData)

    companion object {

        fun findByIndex(index: Int): TutorialInfo {
            return values().find { it.index == index } ?: INTRODUCTION
        }

    }

    enum class Target(@StringRes val message: Int, val condition: (Board) -> Boolean) {
        NONE(-1, { true }),
        PROMOTE_A_UNI(R.string.tutorialTargetReachLastRow, { board: Board ->
            var hasUni = false
            board.eachPiece(null) {
                if(it.type == Type.UNI){
                    hasUni = true
                }
            }
            hasUni
        }),
        CAPTURE(R.string.tutorialTargetCapture, { board: Board ->
            var opponentHasPiece = false
            board.eachPiece(BLACK) {
                opponentHasPiece = true
            }
            !opponentHasPiece
        }),
        WIN_BASELINE(R.string.tutorialTargetWin, { board: Board ->
            board.findKing(WHITE)!!.location.y == WHITE.winningY
        }),
        WIN_CAPTURE(R.string.tutorialTargetWin, { board: Board ->
            board.findKing(BLACK) == null
        }),
    }

}